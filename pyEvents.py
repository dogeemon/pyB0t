#IMPORTS
import pyBot
import pyDebug
import threading

#API ATTACHMENT EVENT
def apiAttach(status):
    if status == 3:  #apiAttachNotAvailable
        debug.errorExit('Skype4Py API has failed to connect.')
    elif status == 1:  #apiAttachPendingAuthorization
        debug.action('Skype4Py API is pending authorization...')
    elif status == 2:  #apiAttachRefused
        debug.errorExit('Skype4Py API has failed to connect.')
    elif status == 0:  #apiAttachSuccess
        debug.alert('Skype4Py API has been connected.')
    elif status == -1: #apiAttachUnknown
        debug.errorExit('Skype4Py API has failed to connect')

#DTMF COODE RECEIVED EVENT
def DTMF(call, code):
    debug.action('DTMF CODE : ' + code)

#MESSAGE EVENT
def onMessage(msg, status):
    commands.handle(msg, status).start()
    
