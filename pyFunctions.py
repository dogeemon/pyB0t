#IMPORTS
import pyConf
import pyDebug
import hashlib
import os
import random
import re
import urllib2
def loadBanAdm():
 pyConf.admins = [line.strip() for line in open(pyConf.fileAlist, 'r')]
 pyConf.banned = [line.strip() for line in open(pyConf.fileBanned,'r')]


def checkDirs():
#Check Dir Configs
 if not os.path.exists(pyConf.dirConfigs):
    pyDebug.action(pyConf.dirConfigs+" doesn't exist. Creating...")
    try:
     os.makedirs(pyConf.dirConfigs)
     pyDebug.action(pyConf.dirConfigs+" Created!")
    except:
     pyDebug.errorExit("Sorry couldn't create '"+pyConf.dirConfigs+'! Quitting...')
 else:
    pyDebug.action(pyConf.dirConfigs+" OK!") 
#Check Dir Logs 
 if not os.path.exists(pyConf.dirLogs):
    pyDebug.action(pyConf.dirLogs+" doesn't exist. Creating...")
    try:
     os.makedirs(pyConf.dirLogs)
     pyDebug.action(pyConf.dirLogs+" Created!")
    except:
     pyDebug.errorExit("Sorry couldn't create '"+pyConf.dirLogs+'! Quitting...')
 else:
    pyDebug.action(pyConf.dirLogs+" OK!") 
#Check Dir Notes 
 if not os.path.exists(pyConf.dirNotes):
    pyDebug.action(pyConf.dirNotes+" doesn't exist. Creating...")
    try:
     os.makedirs(pyConf.dirNotes)
     pyDebug.action(pyConf.dirNotes+" Created!")
    except:
     pyDebug.errorExit("Sorry couldn't create '"+pyConf.dirNotes+'! Quitting...')
 else:
    pyDebug.action(pyConf.dirNotes+" OK!")  
#Check Dir Dox
 if not os.path.exists(pyConf.dirDox):
    pyDebug.action(pyConf.dirDox+" doesn't exist. Creating...")
    try:
     os.makedirs(pyConf.dirDox)
     pyDebug.action(pyConf.dirDox+" Created!")
    except:
     pyDebug.errorExit("Sorry couldn't create '"+pyConf.dirDox+'! Quitting...')
 else:
    pyDebug.action(pyConf.dirDox+" OK!") 
#Check Dir Admin
 if not os.path.exists(pyConf.dirAdmin):
    pyDebug.action(pyConf.dirAdmin+" doesn't exist. Creating...")
    try:
     os.makedirs(pyConf.dirAdmin)
     pyDebug.action(pyConf.dirAdmin+" Created!")
    except:
     pyDebug.errorExit("Sorry couldn't create '"+pyConf.dirAdmin+'! Quitting...')
 else:
    pyDebug.action(pyConf.dirAdmin+" OK!")   



#adFly 
def adfLY(url, convert=True):
    filtersList = ['boards.4chan.org', 'tinychat', 'imgur.com', 'youtu', 'facebook.com', 'gyazo.com', '.bmp', '.gif', '.jpg', '.png']
    for item in filtersList:
        if item in url:
            convert = False
            break
        else:
            pass
    if convert == True:
        try:
            #Please use this sign-up referal [http://adf.ly/?id=4363464] if you are going to use your own api key, it can earn you/me extra money!
            return urllib2.urlopen('http://api.adf.ly/api.php?key=00384da0cb72b552bac9b0a28ae9f375&uid=4363464&advert_type=int&domain=adf.ly&url=' + url).read()
        except:
            return False
    else:
        return False
    
#LOAD DATABASE
def databaseLoad():
    adsFile = open('\Configs\Ads.txt', 'r')
    for line in adsFile.readlines():
        ad = line.replace('\n', '', 1)
        pyConf.adList.append(ad)
    pyConf.adList.sort()
    adsFile.close()
    filterFile = open('\Configs\Filter.txt', 'r')
    for line in filterFile.readlines():
        filters = line.replace('\n', '', 1)
        pyConf.filterList.append(filters)
    pyConf.filterList.sort()
    filterFile.close()

#GET BETWEEN
def getBetween(source, start, stop):
    data = re.compile(start + '(.*?)' + stop).search(source)
    if data:
        found = data.group(1)
        return found.replace('\n', '')
    else:
        return False

#GET CLEAN URL
def getCleanURL(url):
    cleanURL = url.replace('https://', '', 1)
    cleanURL = cleanURL.replace('http://', '', 1)
    cleanURL = cleanURL.replace('www.', '', 1)
    cleanURL = 'http://' + cleanURL
    return cleanURL

#GET RANDOM
def getRandom(length):
    charSet    = 'abcdefghijklmnopqrstuvwxyz'
    randomStr  = ''.join(random.sample(charSet, length))
    return randomStr

#GET TITLE
def getTitle(url):
    try:
        source = urllib2.urlopen(url).read()
        title  = getBetween(source, '<title>', '</title>')
        return title
    except:
        return False


#MD5
def md5(string):
    return hashlib.md5(string).hexdigest()

def setLast(string, body):
    pyConf.lastHandle = string
    pyConf.lastMsg = body
#TINYURL
def tinyURL(url):
    try:
        return urllib2.urlopen('http://tinyurl.com/api-create.php?url=' + url).read()
    except:
        return False
#UPDATECHECK
def updateCheck():
    source = urllib2.urlopen('https://gitlab.com/Nameless59/pyB0t/blob/master/Version').read()
    latest = getBetween(source, 'pyBot', '.')
    splitCurrent = pyConf.version.split('.')
    splitLatest  = latest.split('.')
    if int(splitCurrent[0]) == int(splitLatest[0]):
        if int(splitCurrent[1]) == int(splitLatest[1]):
            if int(splitCurrent[2]) == int(splitLatest[2]):
                pyDebug.alert('You are using the latest updated version of pyBot.')
            elif int(splitCurrent[2]) < int(splitLatest[2]):
                error('Update to version ' + latest + ' now!')
            else:
                pyDebug.alert('You are using the latest updated version of pyBot.')
        elif int(splitCurrent[1]) < int(splitLatest[1]):
            error('Update to version ' + latest + ' now!')
        else:
            pyDebug.alert('You are using the latest updated version of pyBot.')
    elif int(splitCurrent[0]) < int(splitLatest[0]):
        error('Update to version ' + latest + ' now!')
    else:
        pyDebug.alert('You are using the latest updated version of pyBot.')
        
        
