import os

# dir location
cleanDir = os.getcwd()
cleanDir = cleanDir.strip()

dirCommands = os.getcwd() + '\Commands'
dirConfigs = os.getcwd() + '\Configs'
dirAdmin = os.getcwd() + '\Admin'
dirDox = os.getcwd() + '\Dox'
dirNotes = os.getcwd() + '\Notes'
dirLogs = os.getcwd() + '\Logs'
dirMega = os.getcwd() + '\Mega'
dirMegaFiles = dirMega + '\Files'
dirKeys = os.getcwdu() + '\Keys'
# files location
fileLogs = dirLogs + '\Logging.logs'
fileAlist = dirAdmin + '\Alist.txt'
fileACmd = dirAdmin + '\AdmCmd.txt'
fileBanned = dirConfigs + '\Banned.txt'
fileCommand = dirConfigs + '\Commands.txt'
fileICommand = dirConfigs + '\ICommands.txt'
fileTos = dirConfigs + '\Tos.txt'
fileCredits = dirConfigs + '\Credits.txt'
fileSuggerimenti = dirConfigs + '\Suggerimenti.txt'
fileFilter = dirConfigs + '\Filter.txt'
fileAds = dirConfigs + '\Ads.txt'
fileDbase = dirLogs + '\Database.txt'
fileMegaH = dirMega + '\MCommands.txt'
filePremium = dirConfigs + '\PUsers.txt'
fileKeys = dirKeys + '\Keys.txt'

# stufffile
admins = []
banned = []
pusers = []
logtofile = 'true'
adList = []
filterList = []
bots = []
apikey = 'yourapikey'
apibase = 'yourapibase'
apikey2 = 'yourapikey2'
memail = 'yourmegamail'
mpass = 'yourmegapass'
usercmds = []
premiumcmds = []
admincmds = []
premiumKeys = []

# info
coder = 'Carb0n'
name = 'pyBot'
version = '2.1'

# conf
adCount = 0
cmd = []
dbase = []
debug = 'false'
lastMSG = ''
lastHandle = ''
mode = 'chat'
trigger = '!'
