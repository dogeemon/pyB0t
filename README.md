# pyB0t 1.1.3 #
# Based on Plexus #
### What is this repository for? ###

This is a skypebot with:

* Chat commands
* Banning system
* Admin management
* Logging system

***

# How do I get set up? #

* Install [Python 2.*](https://www.python.org/download/releases/2.7/) or newer(i use 2.7.10)
* Download and install [Pip](https://pypi.python.org/packages/source/p/pip/pip-7.1.2.tar.gz#md5=3823d2343d9f3aaab21cf9c917710196)
* Download and extract this project
* Run 'Install.bat'
* When finished run 'Start.bat'
* pyBot will ask you the permissions to connect to Skype([Example](http://i.imgur.com/RnMX60X.png)
* Accept it
* Done!
# More: #

* [Skype4py Documentation](skype4py.sourceforge.net/doc/html/)

* Little Skype4py [tutorial](http://www.mchacks.altervista.org/showthread.php?tid=352) or [this tut](https://leakforums.net/thread-641974)

* Need help? Contact me on [skype](skype:live:nameless59_1?add)


### Chat Commands ###

Available commands:

* help:                    Shows all available commands.
* quote:                   Returns a random famous quote.
* flirt:                   Returns a flirty comments.
* snap:                    Snap a random snapchat nude.
* google <google>:         Google something for you.
* ping:                    Unuseful command.
* joke:                    Returns a random chuck norries joke.
* dice:                    Rolls dice.
* tube <tube>:             Search on youtube for you.
* 8ball <quest>:           It will answer some mystic questions.
* define <word>:           Define a word by searching on vocabulary.
* commands:                Shows all available commands.
* getavatar <skype>:       Get someone's skype Avatar.
* pwgen <long>:            Generate a password for you(max 64 chars).
* resolve <skype>:         Attempts to resolve an skype username.
* ip2skype <ip>:           Checks if there are any user bind to this is.
* ip2host <ip>:            Attempts to get an host from an ip.
* slap <someone>:          Slap someone's ass.
* lookup <skype>:          Lookus up for pasts user's 
* kiss <someone>:          Gives a kiss to someone.
* email2skype <email>:     Attempts to retrieve an skype ID from an email.
* bestemmia:               Invoke god <3.
* name2uuid <name>:        Convert a username into a Mojang UUID.
* webresolve <host>:       Attempts to get an ip from a host.
* homofize <name>:         Troll someone
* geoip <ip>:              Retrieves information about an ip.
* udp <ip port size time>: DDosS someone for you.
* mcalt:                   Generate a Minecraft premium alts(May not always work).
* cmds:                    Shows all available commands.
* maometto:                Invoke cancaro man.
* violento <someone>:      Lyrics a cool song.
* credits:                 Shows the credits.
* suggerisci <testo>:      Suggest something.
* language <ita/eng>:      Change the language.
* shortmyurl<url>:         Shorten your url.
* newnote<text>:           Create a new .txt file that can be read only from you.
* mynote:                  Show your note.
* editnote<text>:          Edit your previous created note.
* reverse<text>:           Reverse your text(EX: elpmaxe)
* terms:                   Show usage terms and contidions.
* md5:                     Encode the message into a md5 hash.
* sha256:                  Encode the message into a sha256 hash.
* ladmins:                 Show the list of admins.
* screensite<url>:         Take a screenshot of the url you want.
* info:                    Show some infos about who is running the bot

***