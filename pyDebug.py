# -*- coding: latin-1 -*-
import pyConf
import pyFunctions
import os
import sys


def action(msg):
  print '[#] - ' + msg
  if pyConf.logtofile == 'true':
    log = open(pyConf.fileLogs,'r')
    conte = log.read()
    log.close             
    log = open(pyConf.fileLogs,'w')
    log.write(conte +'\n' +'[#] - ' + msg)
    log.close


def alert(msg):
  print '[+] - ' + msg
  if pyConf.logtofile == 'true':
    log = open(pyConf.fileLogs,'r')
    conte = log.read()
    log.close             
    log = open(pyConf.fileLogs,'w')
    log.write(conte +'\n' +'[#] - ' + msg)
    log.close

def error(msg):
  print '[!] - ' + msg
  if pyConf.logtofile == 'true':
    log = open(pyConf.fileLogs,'r')
    conte = log.read()
    log.close             
    log = open(pyConf.fileLogs,'w')
    log.write(conte +'\n' +'[#] - ' + msg)
    log.close

def errorExit(msg):
  error(msg)
  raw_input('\nPress the <ENTER> key to EXIT...')
  sys.exit()
  if pyConf.logtofile == 'true':
    log = open(pyConf.fileLogs,'r')
    conte = log.read()
    log.close             
    log = open(pyConf.fileLogs,'w')
    log.write(conte +'\n' +'[#] - ' + msg)
    log.close

def info():
    print '******************************************************************************'
    print 'Name: pyBot'
    print 'Version: ' + pyConf.version
    print 'Coded by Carb0n in Python'
    print 'File     : pyBot.py'
    print "                    ,---.          |    ,--.                   "
    print "                    |    ,---.,---.|---.|  |,---.              "
    print "                    |    ,---||    |   ||  ||   |              "
    print "                    `---'`---^`    `---'`--'`   'SkypeBot      "
    print "                                                               "
    print '******************************************************************************'
def prompt(msg):
    return raw_input('[?] - ' + msg + ' : ')


def versionCheck():
    if sys.version_info < (2, 7, 0):
        errorExit('Requires Python version 2.7 or higher. Quitting...')
    else:
        pass
