# -*- coding: latin-1 -*-
import smtplib
import Skype4Py
import re
import hashlib
import urllib
import getpass
import urllib2
import xml
import platform
from sys import exit
from sgmllib import SGMLParser
import time
import json
import random
import sys, os
import codecs
import threading
import datetime
import Queue
import pyConf
import pyDebug
import pyFunctions
from HTMLParser import HTMLParser
skype = Skype4Py.Skype();
G=datetime.datetime.now().strftime('%d') 
N=datetime.datetime.now().strftime('%m') 
H=datetime.datetime.now().strftime('%H') 
M=datetime.datetime.now().strftime('%M') 

class MLStripper(HTMLParser):


    def __init__(self):
        self.reset()
        self.fed = []
    def handle_data(self, d):
        self.fed.append(d)
    def get_data(self):
        return ''.join(self.fed)
 
def strip_tags(html):
    s = MLStripper()
    s.feed(html)
    return s.get_data()
def getHandle():
    return getAPI().CurrentUser.Handle
def getBetween(strSource, strStart,strEnd):
        start = strSource.find(strStart) + len(strStart)
        end = strSource.find(strEnd,start)
        return strSource[start:end]
 
class SkypeBot:
 
 
        def __init__(self):
                self.skype = Skype4Py.Skype(Events=self)
                self.skype.Attach()
 
        def AttachmentStatus(self, status):
                if status == Skype4Py.apiAttachAvailable:
                        self.skype.attach()
                        
        def MessageStatus(self, message, status):
                   if status == Skype4Py.cmsReceived or status == Skype4Py.cmsSent:
                        splitMessage = message.Body.split()
                        for command, function in self.commands.items():
                                if command in splitMessage[0]:
                                        function(self, message)
                                        break  
        def commandSendall(self, message):
               ips = message.Body.split('sendall')
               ip = ips[1].strip()
               if message.Sender.Handle in pyConf.admins:
                 pyDebug.action('!sendall command executed by '+message.Sender.Handle + '  time: '+ H+':'+M+' of '+G+'/'+N)          
                 for friend in self.skype.Friends:
                     if friend.OnlineStatus != 'OFFLINE':
                             try:
                                message.Chat.SendMessage(friend.Handle,ip +'\n (start) Sent using '+pyConf.name +' Version: '+pyConf.version+'(star)')
                             except:
                                pass  
        def commandJoke(self, message):
          if message.Sender.Handle not in pyConf.banned:
            pyDebug.action('!joke command executed by '+message.Sender.Handle + '  time: '+ H+':'+M+' of '+G+'/'+N)
            admin = open('Admin/Admin.txt','r')
            cont = admin.read()
            admin.close
            if cont == 'true':
               if message.Sender.Handle in pyConf.admins:
                shitt = urllib2.urlopen('http://api.predator.wtf/joke/?arguments=nerdy').read()
                message.Chat.SendMessage('/me'+shitt)
               else:
                message.Chat.SendMessage('/me You do not have permission to do this')
            else:
                shitt = urllib2.urlopen('http://api.predator.wtf/joke/?arguments=nerdy').read()
                message.Chat.SendMessage('/me'+shitt)
        def commandFml(self,message):
          if message.Sender.Handle not in pyConf.banned:
            pyDebug.action('!fml command executed by '+message.Sender.Handle + '  time: '+ H+':'+M+' of '+G+'/'+N)
            admin = open('Admin/Admin.txt','r')
            cont = admin.read()
            admin.close
            if cont == 'true':
               if message.Sender.Handle in pyConf.admins:
                dataa = urllib2.urlopen('http://m.fmylife.com/random').read()
                fml = getBetween(dataa,'<p class="text">','</p>')
                message.Chat.SendMessage(strip_tags('/me '+fml))
               else:
                message.Chat.SendMessage('/me You do not have permission to do this')
            else:
                dataa = urllib2.urlopen('http://m.fmylife.com/random').read()
                fml = getBetween(dataa,'<p class="text">','</p>')
                message.Chat.SendMessage(strip_tags('/me '+fml))
        def commandUrban(self, message):
          if message.Sender.Handle not in pyConf.banned:
            pyDebug.action('!define command executed by '+message.Sender.Handle + '  time: '+ H+':'+M+' of '+G+'/'+N)
            admin = open('Admin/Admin.txt','r')
            cont = admin.read()
            admin.close
            if cont == 'true':
               if message.Sender.Handle in pyConf.admins:
                urban = message.Body.split('define')
                word = urban[1].strip()
                r  = urllib2.urlopen('http://api.urbandictionary.com/v0/define?term=' + word)
                data = json.loads(r.read())
                if ( len(data['list']) > 1 ):
                      data['list'] = data['list'][:1]  # only print 2 results
                      for i in range(len(data['list'])):
                        word = data['list'][i][u'word']
                        definition = data['list'][i][u'definition']
                        example = data['list'][i][u'example']
                        permalink = data['list'][i][u'permalink']
                        message.Chat.SendMessage('/me '+word+': ' + definition),
                        message.Chat.SendMessage('/me Example: ' + example)
                else:
                        message.Chat.SendMessage('Word not found.')
                       
               else:
                message.Chat.SendMessage('/me You do not have permission to do this')
            else:
                urban = message.Body.split('define')
                word = urban[1].strip()
                r  = urllib2.urlopen('http://api.urbandictionary.com/v0/define?term=' + word)
                data = json.loads(r.read())
                if ( len(data['list']) > 1 ):
                      data['list'] = data['list'][:1]  # only print 2 results
                      for i in range(len(data['list'])):
                        word = data['list'][i][u'word']
                        definition = data['list'][i][u'definition']
                        example = data['list'][i][u'example']
                        permalink = data['list'][i][u'permalink']
                        message.Chat.SendMessage('/me '+word+': ' + definition),
                        message.Chat.SendMessage('/me Example: ' + example)
                else:
                        message.Chat.SendMessage('Word not found.')
                       
        def command8ball(self, message):
          if message.Sender.Handle not in pyConf.banned:
            pyDebug.action('!8ball command executed by '+message.Sender.Handle + '  time: '+ H+':'+M+' of '+G+'/'+N)
            admin = open('Admin/Admin.txt','r')
            cont = admin.read()
            admin.close
            if cont == 'true':
               if message.Sender.Handle in pyConf.admins:
                splitMessage = message.Body.split(' ',1)
                messageText = splitMessage[1]
                Choice = ['It is certain',
'It is decidedly so',
'Without a doubt',
'Yes definitely',
'You may rely on it',
'As I see it, yes',
'Most likely',
'Outlook good',
'Yes',
'Signs point to yes',
'Reply hazy try again',
'Ask again later',
'Better not tell you now',
'Cannot predict now',
'Concentrate and ask again',
'Don"t count on it',
'My reply is no',
'My sources say no',
'Outlook not so good',
'Very doubtful']
                message.Chat.SendMessage('/me '+Choice[random.randint(0,11)])
               else:
                message.Chat.SendMessage('/me You do not have permission to do this')
            else:
                splitMessage = message.Body.split(' ',1)
                messageText = splitMessage[1]
                Choice = ['It is certain',
'It is decidedly so',
'Without a doubt',
'Yes definitely',
'You may rely on it',
'As I see it, yes',
'Most likely',
'Outlook good',
'Yes',
'Signs point to yes',
'Reply hazy try again',
'Ask again later',
'Better not tell you now',
'Cannot predict now',
'Concentrate and ask again',
'Don"t count on it',
'My reply is no',
'My sources say no',
'Outlook not so good',
'Very doubtful']
                time.sleep(2)
                message.Chat.SendMessage('/me '+Choice[random.randint(0,11)])
        def commandTube(self, message):
          if message.Sender.Handle not in pyConf.banned:
            pyDebug.action('!tube command executed by '+message.Sender.Handle + '  time: '+ H+':'+M+' of '+G+'/'+N)
            admin = open('Admin/Admin.txt','r')
            cont = admin.read()
            admin.close
            if cont == 'true':
               if message.Sender.Handle in pyConf.admins:
                        global Finder
                        global Finder2
                        splitMessage = message.Body.split(' ',1)
                        Finder = message.Body.split(' ',5)
                        Finder2 = message.Body.split(' ',5)
                        messageText = splitMessage[1]
                        SplitSearch = splitMessage[1].replace(' ','+')
                        URL = urllib2.urlopen('http://www.youtube.com/results?q='+SplitSearch).read()
                        String = re.findall(r'\/watch\?v=\w{11}',URL)
                        Array = []
                        for x in String:
                                if x in Array:
                                        pass
                                else:
                                        Array.append(x)
                        #print Array
                        message.Chat.SendMessage('/me Searching (3) Results for: '+messageText)
                        for i in Array[0:3]:
                                time.sleep(2)
                                message.Chat.SendMessage('/me http://youtube.com'+i)
               else:
                message.Chat.SendMessage('/me You do not have permission to do this')
            else:
                        splitMessage = message.Body.split(' ',1)
                        Finder = message.Body.split(' ',5)
                        Finder2 = message.Body.split(' ',5)
                        messageText = splitMessage[1]
                        SplitSearch = splitMessage[1].replace(' ','+')
                        URL = urllib2.urlopen('http://www.youtube.com/results?q='+SplitSearch).read()
                        String = re.findall(r'\/watch\?v=\w{11}',URL)
                        Array = []
                        for x in String:
                                if x in Array:
                                        pass
                                else:
                                        Array.append(x)
                        #print Array
                        message.Chat.SendMessage('/me Searching (3) Results for: '+messageText)
                        for i in Array[0:3]:
                                time.sleep(2)
                                message.Chat.SendMessage('/me http://youtube.com'+i)
        def commandPing(self, message):
          if message.Sender.Handle not in pyConf.banned:
            pyDebug.action('!ping command executed by '+message.Sender.Handle + '  time: '+ H+':'+M+' of '+G+'/'+N) 
            admin = open('Admin/Admin.txt','r')
            cont = admin.read()
            admin.close
            if cont == 'true':
               if message.Sender.Handle in pyConf.admins:
                message.Chat.SendMessage('/me pong')  
               else:
                message.Chat.SendMessage('/me You do not have permission to do this')
            else:
                message.Chat.SendMessage('/me pong')   
        def commandViolento(self, message):
          if message.Sender.Handle not in pyConf.banned:
            pyDebug.action('!violento command executed by '+message.Sender.Handle + '  time: '+ H+':'+M+' of '+G+'/'+N)
            admin = open('Admin/Admin.txt','r')
            cont = admin.read()
            admin.close
            if cont == 'true':
               if message.Sender.Handle in pyConf.admins:
                splitMessage = message.Body.split('!violento',1)
                messageText = splitMessage[1]
                message.Chat.SendMessage('/me Cara/o ' + messageText + ' ti voglio dedicare una canzone')
                message.Chat.SendMessage('Inizia cosi:')
                time.sleep(1)
                message.Chat.SendMessage('/me Io ti violento e poi ti vengo dentro')
                time.sleep(2)
                message.Chat.SendMessage('/me Io ti violento e poi ti vengo dentro e son contento! ')
                time.sleep(2)
                message.Chat.SendMessage('/me Io ti violento anche se sei in carrozzina')
                time.sleep(1)
                message.Chat.SendMessage('/me Io ti violento ti metto a pecorina')
                time.sleep(2)
                message.Chat.SendMessage('/me Io ti violento e poi ti vengo dentro e son contento!')
                time.sleep(2)
                message.Chat.SendMessage('/me Io ti violento anche se hai 4 anni')
                time.sleep(2)
                message.Chat.SendMessage('/me Io ti violento anche se ancora non parli')                
                time.sleep(2)
                message.Chat.SendMessage('/me Io ti violento e poi ti vengo dentro e son contento!')     
                time.sleep(2)
                message.Chat.SendMessage('/me Io ti violento anche se sei sordomuta')
                time.sleep(2)
                message.Chat.SendMessage('/me Io ti violento anche se sei non vedente')               
                time.sleep(2)
                message.Chat.SendMessage('/me Io ti violento anche se non hai gambe braccia non fa niente!')
                time.sleep(2)      
                message.Chat.SendMessage('/me Io ti violento e poi ti vengo dentro e son contento!!')
               else:
                message.Chat.SendMessage('/me You do not have permission to do this')
            else:
                splitMessage = message.Body.split('!violento',1)
                messageText = splitMessage[1]
                message.Chat.SendMessage('/me Cara/o ' + messageText + ' ti voglio dedicare una canzone')
                message.Chat.SendMessage('Inizia cosi:')
                time.sleep(1)
                message.Chat.SendMessage('/me Io ti violento e poi ti vengo dentro')
                time.sleep(2)
                message.Chat.SendMessage('/me Io ti violento e poi ti vengo dentro e son contento! ')
                time.sleep(2)
                message.Chat.SendMessage('/me Io ti violento anche se sei in carrozzina')
                time.sleep(1)
                message.Chat.SendMessage('/me Io ti violento ti metto a pecorina')
                time.sleep(2)
                message.Chat.SendMessage('/me Io ti violento e poi ti vengo dentro e son contento!')
                time.sleep(2)
                message.Chat.SendMessage('/me Io ti violento anche se hai 4 anni')
                time.sleep(2)
                message.Chat.SendMessage('/me Io ti violento anche se ancora non parli')                
                time.sleep(2)
                message.Chat.SendMessage('/me Io ti violento e poi ti vengo dentro e son contento!')     
                time.sleep(2)
                message.Chat.SendMessage('/me Io ti violento anche se sei sordomuta')
                time.sleep(2)
                message.Chat.SendMessage('/me Io ti violento anche se sei non vedente')               
                time.sleep(2)
                message.Chat.SendMessage('/me Io ti violento anche se non hai gambe braccia non fa niente!')
                time.sleep(2)      
                message.Chat.SendMessage('/me Io ti violento e poi ti vengo dentro e son contento!!')
               
        def commandDice(self, message):
          if message.Sender.Handle not in pyConf.banned:
            pyDebug.action('!dice command executed by '+message.Sender.Handle + '  time: '+ H+':'+M+' of '+G+'/'+N)
            admin = open('Admin/Admin.txt','r')
            cont = admin.read()
            admin.close
            if cont == 'true':
               if message.Sender.Handle in pyConf.admins:
                message.Chat.SendMessage('/me Put a bet on numbers 1 through 6.')
                time.sleep(8)
                rolled = random.randint(1,6)
                message.Chat.SendMessage('/me *rolls dice*')
                time.sleep(1)
                message.Chat.SendMessage('/me The dice rolled the number:')
                rolled = str(rolled)
                message.Chat.SendMessage('/me '+rolled)
               else:
                message.Chat.SendMessage('/me You do not have permission to do this')
            else:
                message.Chat.SendMessage('/me Put a bet on numbers 1 through 6.')
                time.sleep(8)
                rolled = random.randint(1,6)
                message.Chat.SendMessage('/me *rolls dice*')
                time.sleep(1)
                message.Chat.SendMessage('/me The dice rolled the number:')
                rolled = str(rolled)
                message.Chat.SendMessage('/me '+rolled)
        def commandSay(self, message):
          if message.Sender.Handle not in pyConf.banned:
            pyDebug.action('!say command executed by '+message.Sender.Handle + '  time: '+ H+':'+M+' of '+G+'/'+N)
            admin = open('Admin/Admin.txt','r')
            cont = admin.read()
            admin.close
            if cont == 'true':
               if message.Sender.Handle in pyConf.admins:
                splitMessage = message.Body.split(' ',1)
                messageText = splitMessage[1]
                if messageText != '!say':
                 message.Chat.SendMessage('/me '+messageText)
                else:
                 message.Chat.SendMessage('/me Cazzo volevi fare eh?')
               else:
                message.Chat.SendMessage('/me You do not have permission to do this')
            else:
                splitMessage = message.Body.split(' ',1)
                messageText = splitMessage[1]
                if messageText != '!say':
                 message.Chat.SendMessage('/me '+messageText)
                else:
                 message.Chat.SendMessage('/me Cazzo volevi fare eh?')
        def commandGoogle(self, message):
          if message.Sender.Handle not in pyConf.banned:
            pyDebug.action('!google command executed by '+message.Sender.Handle + '  time: '+ H+':'+M+' of '+G+'/'+N)
            admin = open('Admin/Admin.txt','r')
            cont = admin.read()
            admin.close
            if cont == 'true':
               if message.Sender.Handle in pyConf.admins:
                splitMessage = message.Body.split(' ',1)
                messageText = splitMessage[1]
                message.Chat.SendMessage('/me http://lmgtfy.com/?q='+messageText)
               else:
                message.Chat.SendMessage('/me You do not have permission to do this')
            else:
                splitMessage = message.Body.split(' ',1)
                messageText = splitMessage[1]
                message.Chat.SendMessage('/me http://lmgtfy.com/?q='+messageText)
        def commandResolve(self, message):
          if message.Sender.Handle not in pyConf.banned:
            pyDebug.action('!resolve command executed by '+message.Sender.Handle + '  time: '+ H+':'+M+' of '+G+'/'+N)
            admin = open('Admin/Admin.txt','r')
            cont = admin.read()
            admin.close

            ips = message.Body.split('resolve')
            ip = ips[1].strip()
            if cont == 'true' and ip != 'name.less59':
               if message.Sender.Handle in pyConf.admins:
                message.Chat.SendMessage('/me Grabbing Latest IP for the user ['+ip+'].')
                message.Chat.SendMessage('/me API1: Resolving...\n API2: Resolving...\n API3: Resolving...\nAPI4: Resolving...')
                msg = self.Message.Body
                req = urllib2.Request('http://rooting.xyz/resolver1.php?key=yourapikeyhere&name=' + ip, None, {'User-Agent': 'Mozilla/5.0'})
                response = urllib2.urlopen(req).read()
                msg.replace('/me API1: '+response+'\n API2: Resolving...\n API3: Resolving...\nAPI4: Resolving...','/me API1: Resolving...\n API2: Resolving...\n API3: Resolving...\nAPI4: Resolving...')
                req1 = urllib2.Request('http://rooting.xyz/resolver2.php?key=yourapikeyhere&name=' + ip, None, {'User-Agent': 'Mozilla/5.0'})
                response1 = urllib2.urlopen(req1).read()
          
                req2 = urllib2.Request('http://rooting.xyz/resolver3.php?key=yourapikeyhere&name=' + ip, None, {'User-Agent': 'Mozilla/5.0'})
                response2 = urllib2.urlopen(req2).read()

                req4 = urllib2.Request('http://resolv.heater-down.eu/a1/skype/resolver.php?key=yourapikeyhere&pseudo=' + ip, None, {'User-Agent': 'Mozilla/5.0'})
                response4 = urllib2.urlopen(req4).read()

               else:
                message.Chat.SendMessage('/me You do not have permission to do this')
            else:
                ips = message.Body.split('resolve')
                ip = ips[1].strip()
                message.Chat.SendMessage('/me Grabbing Latest IP for the user ['+ip+'].')
                req = urllib2.Request('http://rooting.xyz/resolver1.php?key=yourapikeyhere&name=' + ip, None, {'User-Agent': 'Mozilla/5.0'})
                response = urllib2.urlopen(req).read()
                message.Chat.SendMessage('/me API1:%s' % response)
                req1 = urllib2.Request('http://rooting.xyz/resolver2.php?key=yourapikeyhere&name=echo123' + ip, None, {'User-Agent': 'Mozilla/5.0'})
                response1 = urllib2.urlopen(req1).read()
                message.Chat.SendMessage('/me API2:%s' % response1)            
                req2 = urllib2.Request('http://rooting.xyz/resolver3.php?key=yourapikeyhere&name=' + ip, None, {'User-Agent': 'Mozilla/5.0'})
                response2 = urllib2.urlopen(req2).read()
                message.Chat.SendMessage('/me API3:%s' % response2)
                req3 = urllib2.Request('http://resolv.heater-down.eu/a1/skype/resolver.php?key=yourapikeyhere&pseudo=' + ip, None, {'User-Agent': 'Mozilla/5.0'})
                response3 = urllib2.urlopen(req3).read()
                message.Chat.SendMessage('/me API4:%s' % response3)
        def commandWebres(self, message):
          if message.Sender.Handle not in pyConf.banned:
            pyDebug.action('!webresolve command executed by '+message.Sender.Handle + '  time: '+ H+':'+M+' of '+G+'/'+N)
            admin = open('Admin/Admin.txt','r')
            cont = admin.read()
            admin.close
            if cont == 'true':
               if message.Sender.Handle in pyConf.admins:
                ips = message.Body.split('webresolve')
                ip = ips[1].strip()
                req = urllib2.Request('http://rooting.xyz/host2ip.php?key=yourapikeyhere&host=' + ip, None, {'User-Agent': 'Mozilla/5.0'})
                response = urllib2.urlopen(req).read()
                message.Chat.SendMessage('/me Generating data on the host ['+ip+'].')
                message.Chat.SendMessage('/me %s' % response)
               else:
                message.Chat.SendMessage('/me You do not have permission to do this')
            else:
                ips = message.Body.split('webresolve')
                ip = ips[1].strip()
                req = urllib2.Request('http://rooting.xyz/host2ip.php?key=yourapikeyhere&host=' + ip, None, {'User-Agent': 'Mozilla/5.0'})
                response = urllib2.urlopen(req).read()
                message.Chat.SendMessage('/me Generating data on the host ['+ip+'].')
                message.Chat.SendMessage('/me %s' % response)
        def commandPwgen(self, message):
          if message.Sender.Handle not in pyConf.banned:
            pyDebug.action('!pwgen command executed by '+message.Sender.Handle + '  time: '+ H+':'+M+' of '+G+'/'+N)
            admin = open('Admin/Admin.txt','r')
            cont = admin.read()
            admin.close
            if cont == 'true':
               if message.Sender.Handle in pyConf.admins:
                ips = message.Body.split('pwgen')
                ip = ips[1].strip()
                req = urllib2.Request('http://api.predator.wtf/passgen/?arguments=' + ip, None, {'User-Agent': 'Mozilla/5.0'})
                response = urllib2.urlopen(req).read()
                time.sleep(2)
                message.Chat.SendMessage('/me Password: %s' % response)
               else:
                message.Chat.SendMessage('/me You do not have permission to do this')
            else:
                ips = message.Body.split('pwgen')
                ip = ips[1].strip()
                req = urllib2.Request('http://api.predator.wtf/passgen/?arguments=' + ip, None, {'User-Agent': 'Mozilla/5.0'})
                response = urllib2.urlopen(req).read()
                time.sleep(2)
                message.Chat.SendMessage('/me Password: %s' % response)
        def commandSlap(self, message):
          if message.Sender.Handle not in pyConf.banned:
            pyDebug.action('!slap command executed by '+message.Sender.Handle + '  time: '+ H+':'+M+' of '+G+'/'+N)
            admin = open('Admin/Admin.txt','r')
            cont = admin.read()
            admin.close
            if cont == 'true':
               if message.Sender.Handle in pyConf.admins:
                ips = message.Body.split('slap')
                ip = ips[1].strip()
                message.Chat.SendMessage('/me has slapped '+ip+'"s ass. ;)')
               else:
                message.Chat.SendMessage('/me You do not have permission to do this')
            else:
                ips = message.Body.split('slap')
                ip = ips[1].strip()
                message.Chat.SendMessage('/me has slapped '+ip+'"s ass. ;)')
        def commandKiss(self, message):
          if message.Sender.Handle not in pyConf.banned:
            pyDebug.action('!kiss command executed by '+message.Sender.Handle + '  time: '+ H+':'+M+' of '+G+'/'+N)
            admin = open('Admin/Admin.txt','r')
            cont = admin.read()
            admin.close
            if cont == 'true':
               if message.Sender.Handle in pyConf.admins:
                ips = message.Body.split('kiss')
                ip = ips[1].strip()
                message.Chat.SendMessage('/me has given '+ip+' a big fat kiss :*')
               else:
                message.Chat.SendMessage('/me You do not have permission to do this')
            else:
                ips = message.Body.split('kiss')
                ip = ips[1].strip()
                message.Chat.SendMessage('/me has given '+ip+' a big fat kiss :*')
        def commandMaometto(self, message):
          if message.Sender.Handle not in pyConf.banned:
            pyDebug.action('!maometto command executed by '+message.Sender.Handle + '  time: '+ H+':'+M+' of '+G+'/'+N)
            admin = open('Admin/Admin.txt','r')
            cont = admin.read()
            admin.close
            if cont == 'true':
               if message.Sender.Handle in pyConf.admins:
                Choice2 = ['Diocancaroooooo',
'Fermateeee diocan Fermateee diocancarooo',
'I Bocca il papa e tutti i soi filistei',
'Vara vara vara',
'Oeee Putanaa',
'Putaneeeeee Troieeee',
'Dio cancaro de diooo']
                        
                message.Chat.SendMessage('/me '+Choice2[random.randint(0,7)])
               else:
                message.Chat.SendMessage('/me You do not have permission to do this')
            else:
                Choice2 = ['Diocancaroooooo',
'Fermateeee diocan Fermateee diocancarooo',
'I Bocca il papa e tutti i soi filistei',
'Vara vara vara',
'Oeee Putanaa',
'Putaneeeeee Troieeee',
'Dio cancaro de diooo']
                        
                message.Chat.SendMessage('/me '+Choice2[random.randint(0,7)])
        def commandCommands(self, message):
          if message.Sender.Handle not in pyConf.banned:
            pyDebug.action('!help command executed by '+message.Sender.Handle + '  time: '+ H+':'+M+' of '+G+'/'+N)
            admin = open('Admin/Admin.txt','r')
            cont = admin.read()
            admin.close
            if cont == 'true':
               if message.Sender.Handle in pyConf.admins:
                language = open('Configs/Language.txt','r')
                cont = language.read()
                if cont == 'ita':
                 fo = open('Configs/ICommands.txt', 'r')
                 str = fo.read() 
                 fo.close()
                 message.Chat.SendMessage('/me ' + str) 
                else:
                 fo = open('Configs/Commands.txt', 'r')
                 str = fo.read() 
                 fo.close()
                 message.Chat.SendMessage('/me ' + str)
                 language.close
               else:
                message.Chat.SendMessage('/me You do not have permission to do this')
            else:
                language = open('Configs/Language.txt','r')
                cont = language.read()
                if cont == 'ita':
                 fo = open('Configs/ICommands.txt', 'r')
                 str = fo.read() 
                 fo.close()
                 message.Chat.SendMessage('/me ' + str) 
                else:
                 fo = open('Configs/Commands.txt', 'r')
                 str = fo.read() 
                 fo.close()
                 message.Chat.SendMessage('/me ' + str)
                 language.close
        def commandCredits(self, message):
          if message.Sender.Handle not in pyConf.banned:
            pyDebug.action('!credits command executed by '+message.Sender.Handle + '  time: '+ H+':'+M+' of '+G+'/'+N)
            admin = open('Admin/Admin.txt','r')
            cont = admin.read()
            admin.close
            if cont == 'true':
               if message.Sender.Handle in pyConf.admins:
                fo = open('Configs/Credits.txt', 'r')
                str = fo.read() 
                fo.close()
                message.Chat.SendMessage('/me ' + str)
               else:
                message.Chat.SendMessage('/me You do not have permission to do this')
            else:
                fo = open('Configs/Credits.txt', 'r')
                str = fo.read() 
                fo.close()
                message.Chat.SendMessage('/me ' + str)
        def commandGeoU(self, message):
          if message.Sender.Handle not in pyConf.banned:
            pyDebug.action('!geou command executed by '+message.Sender.Handle + '  time: '+ H+':'+M+' of '+G+'/'+N)
            admin = open('Admin/Admin.txt','r')
            cont = admin.read()
            admin.close
            if cont == 'true':
               if message.Sender.Handle in pyConf.admins:
                ips = message.Body.split('geou')
                ip = ips[1].strip()
                message.Chat.SendMessage('/me Grabbing '+ip+'"s ip.')
                req1 = urllib2.Request('http://rooting.xyz/resolver1.php?key=yourapikeyhere&ip=' + ip, None, {'User-Agent': 'Mozilla/5.0'})
                response1 = urllib2.urlopen(req1).read()
                message.Chat.SendMessage('/me Getting informations from '+response1+'...')
                req = urllib2.Request('http://rooting.xyz/geoip.php?key=yourapikeyhere&ip=' + response1, None, {'User-Agent': 'Mozilla/5.0'})
                response = urllib2.urlopen(req).read()
                message.Chat.SendMessage(strip_tags('/me '+response))
            else:
                ips = message.Body.split('geou')
                ip = ips[1].strip()
                message.Chat.SendMessage('/me Grabbing '+ip+'"s ip.')
                req1 = urllib2.Request('http://rooting.xyz/resolver1.php?key=yourapikeyhere&ip=' + ip, None, {'User-Agent': 'Mozilla/5.0'})
                response1 = urllib2.urlopen(req1).read()
                message.Chat.SendMessage('/me Getting informations from '+response1+'...')
                req = urllib2.Request('http://rooting.xyz/geoip.php?key=yourapikeyhere&ip=' + response1, None, {'User-Agent': 'Mozilla/5.0'})
                response = urllib2.urlopen(req).read()
                message.Chat.SendMessage(strip_tags('/me '+response))
        def commandGeo(self, message):
          if message.Sender.Handle not in pyConf.banned:
            pyDebug.action('!geoip command executed by '+message.Sender.Handle + '  time: '+ H+':'+M+' of '+G+'/'+N)
            admin = open('Admin/Admin.txt','r')
            cont = admin.read()
            admin.close
            if cont == 'true':
               if message.Sender.Handle in pyConf.admins:
                ips = message.Body.split('geoip')
                ip = ips[1].strip()
                message.Chat.SendMessage('/me Generating data on the IP ['+ip+'].')
                req = urllib2.Request('http://rooting.xyz/geoip.php?key=yourapikeyhere&ip=' + ip, None, {'User-Agent': 'Mozilla/5.0'})
                response = urllib2.urlopen(req).read()
                time.sleep(2)
                message.Chat.SendMessage(strip_tags('/me '+response))
               else:
                message.Chat.SendMessage('/me You do not have permission to do this')
            else:
                ips = message.Body.split('geoip')
                ip = ips[1].strip()
                message.Chat.SendMessage('/me Generating data on the IP ['+ip+'].')
                req = urllib2.Request('http://rooting.xyz/geoip.php?key=yourapikeyhere&ip=' + ip, None, {'User-Agent': 'Mozilla/5.0'})
                response = urllib2.urlopen(req).read()
                time.sleep(2)
                message.Chat.SendMessage(strip_tags('/me '+response))
        def commandIpHost(self, message):
          if message.Sender.Handle not in pyConf.banned:
            pyDebug.action('!ip2host command executed by '+message.Sender.Handle + '  time: '+ H+':'+M+' of '+G+'/'+N)
            admin = open('Admin/Admin.txt','r')
            cont = admin.read()
            admin.close
            if cont == 'true':
               if message.Sender.Handle in pyConf.admins:
                ips = message.Body.split('ip2host')
                ip = ips[1].strip()
                req = urllib2.Request('http://rooting.xyz/ip2host.php?key=yourapikeyhere&ip=' + ip, None, {'User-Agent': 'Mozilla/5.0'})
                response = urllib2.urlopen(req).read()
                message.Chat.SendMessage('/me Generating data on the IP ['+ip+'].')
                message.Chat.SendMessage('/me '+response)
               else:
                message.Chat.SendMessage('/me You do not have permission to do this')
            else:
                ips = message.Body.split('ip2host')
                ip = ips[1].strip()
                req = urllib2.Request('http://rooting.xyz/ip2host.php?key=yourapikeyhere&ip=' + ip, None, {'User-Agent': 'Mozilla/5.0'})
                response = urllib2.urlopen(req).read()
                message.Chat.SendMessage('/me Generating data on the IP ['+ip+'].')
                message.Chat.SendMessage('/me '+response)
        def commandIpSkype(self, message):
          if message.Sender.Handle not in pyConf.banned:
            pyDebug.action('!ip2skype command executed by '+message.Sender.Handle + '  time: '+ H+':'+M+' of '+G+'/'+N)
            admin = open('Admin/Admin.txt','r')
            cont = admin.read()
            admin.close
            if cont == 'true':
              if message.Sender.Handle in pyConf.admins:
               ips = message.Body.split('ip2skype ')
               ip = ips[1].strip()
               if ip != 'live:nameless59_1':
                req = urllib2.Request('http://rooting.xyz/ip2skype.php?key=yourapikeyhere&ip=' + ip, None, {'User-Agent': 'Mozilla/5.0'})
                response = urllib2.urlopen(req).read()
                message.Chat.SendMessage('/me Generating data on the IP ['+ip+'].')
                message.Chat.SendMessage('/me '+ response)
              else:
               message.Chat.SendMessage('/me You do not have permission to do this')
            else:
               ips = message.Body.split('ip2skype ')
               ip = ips[1].strip()
               if ip != 'live:nameless59_1':
                req = urllib2.Request('http://rooting.xyz/ip2skype.php?key=yourapikeyhere&ip=' + ip, None, {'User-Agent': 'Mozilla/5.0'})
                response = urllib2.urlopen(req).read()
                message.Chat.SendMessage('/me Generating data on the IP ['+ip+'].')
                message.Chat.SendMessage('/me '+ response)
        def commandLookup(self, message):
          if message.Sender.Handle not in pyConf.banned:
            pyDebug.action('!lookup command executed by '+message.Sender.Handle + '  time: '+ H+':'+M+' of '+G+'/'+N)
            admin = open('Admin/Admin.txt','r')
            cont = admin.read()
            admin.close
            if cont == 'true':
               if message.Sender.Handle in pyConf.admins:
                ips = message.Body.split('lookup ')
                ip = ips[1].strip()
                req = urllib2.Request('http://api.predator.wtf/lookup/?arguments=' + ip, None, {'User-Agent': 'Mozilla/5.0'})
                response = urllib2.urlopen(req).read()               
                responsemod = response.replace('Athena Found No Results!','No IP founds')
                message.Chat.SendMessage('/me '+ responsemod)
               else:
                message.Chat.SendMessage('/me You do not have permission to do this')
            else:
                ips = message.Body.split('lookup ')
                ip = ips[1].strip()
                req = urllib2.Request('http://api.predator.wtf/lookup/?arguments=' + ip, None, {'User-Agent': 'Mozilla/5.0'})
                response = urllib2.urlopen(req).read()               
                responsemod = response.replace('Athena Found No Results!','No IP founds')
                message.Chat.SendMessage('/me '+ responsemod)
        def commandCosa(self, message):
          if message.Sender.Handle not in pyConf.banned:
           if message.Sender.Handle != 'live:nameless59_1':
            pyDebug.action('cosa command executed by '+message.Sender.Handle + '  time: '+ H+':'+M+' of '+G+'/'+N)
            admin = open('Admin/Admin.txt','r')
            cont = admin.read()
            admin.close
            if cont == 'true':
               if message.Sender.Handle in pyConf.admins:
                message.Chat.SendMessage('/me Stocazzo!')
               else:
                message.Chat.SendMessage('/me You do not have permission to do this')
            else:
                message.Chat.SendMessage('/me Stocazzo!')
        def commandInsult(self, message):
               pyDebug.action('!spam command executed by '+message.Sender.Handle + '  time: '+ H+':'+M+' of '+G+'/'+N)
               if message.Sender.Handle in pyConf.admins:
                while 1:
                    try:
                     message.Chat.SendMessage('this groupchat is cancer')
                    except Exception,e:
                     message.Chat.SendMessage('spam')
                    else:      
                     message.Chat.SendMessage('skype4py ftw spam spam spam')
        def commandSpam(self, message):
          if message.Sender.Handle not in pyConf.banned:
               pyDebug.action('!cspam command executed by '+message.Sender.Handle + '  time: '+ H+':'+M+' of '+G+'/'+N) 
               if message.Sender.Handle in pyConf.admins:
                ips = message.Body.split('cspam')
                ip = ips[1].strip()
                i = ip;
                fatto = 0;
                while fatto < i:
                    fatto = fatto+1;
                    try:
                     message.Chat.SendMessage('this groupchat is cancer')
                    except Exception,e:         
                     message.Chat.SendMessage('spam')
                    else:      
                     message.Chat.SendMessage('skype4py ftw spam spam spam')
        def commandCarb0n(self, message):
         if message.Sender.Handle not in pyConf.banned:
          if message.Sender.Handle != 'live:nameless59_1':
            pyDebug.action('carb0n command executed by '+message.Sender.Handle + '  time: '+ H+':'+M+' of '+G+'/'+N)
            admin = open('Admin/Admin.txt','r')
            cont = admin.read()
            admin.close
            if cont == 'true':
               if message.Sender.Handle in pyConf.admins:
                message.Chat.SendMessage('/me Che cazzo vuoi?!')
               else:
                message.Chat.SendMessage('/me You do not have permission to do this')
            else:
                message.Chat.SendMessage('/me Che cazzo vuoi?!')
        def commandInsult(self, message):
               pyDebug.action('!spam command executed by '+message.Sender.Handle + '  time: '+ H+':'+M+' of '+G+'/'+N)
               if message.Sender.Handle in pyConf.admins:
                while 1:
                    try:
                     message.Chat.SendMessage('this groupchat is cancer')
                    except Exception,e:
                     message.Chat.SendMessage('spam')
                    else:      
                     message.Chat.SendMessage('skype4py ftw spam spam spam') 

        def commandFlirt(self, message):
          if message.Sender.Handle not in pyConf.banned:
            pyDebug.action('!flirt command executed by '+message.Sender.Handle + '  time: '+ H+':'+M+' of '+G+'/'+N)
            admin = open('Admin/Admin.txt','r')
            cont = admin.read()
            admin.close
            if cont == 'true':
               if message.Sender.Handle in pyConf.admins:
                ips = message.Body.split('flirt')
                ip = ips[1].strip()
                req = urllib2.Request('http://rooting.xyz/flirt.php?key=yourapikeyhere' + ip, None, {'User-Agent': 'Mozilla/5.0'})
                response = urllib2.urlopen(req).read()
                time.sleep(2)
                message.Chat.SendMessage('/me %s' % response)
               else:
                message.Chat.SendMessage('/me You do not have permission to do this')
            else:
                ips = message.Body.split('flirt')
                ip = ips[1].strip()
                req = urllib2.Request('http://rooting.xyz/flirt.php?key=yourapikeyhere' + ip, None, {'User-Agent': 'Mozilla/5.0'})
                response = urllib2.urlopen(req).read()
                time.sleep(2)
                message.Chat.SendMessage('/me %s' % response)
        def commandEmail(self, message):
          if message.Sender.Handle not in pyConf.banned:
            pyDebug.action('!email2skype command executed by '+message.Sender.Handle + '  time: '+ H+':'+M+' of '+G+'/'+N)
            admin = open('Admin/Admin.txt','r')
            cont = admin.read()
            admin.close
            if cont == 'true':
               if message.Sender.Handle in pyConf.admins:
                ips = message.Body.split('email2skype')
                ip = ips[1].strip()
                req = urllib2.Request('http://rooting.xyz/email2skype.php?key=yourapikeyhere&email=' + ip, None, {'User-Agent': 'Mozilla/5.0'})
                response = urllib2.urlopen(req).read()
                message.Chat.SendMessage('/me API1: %s' % response)
                req1 = urllib2.Request('http://api.predator.wtf/e2skype/?arguments=' + ip, None, {'User-Agent': 'Mozilla/5.0'})
                response1 = urllib2.urlopen(req1).read()
                message.Chat.SendMessage('/me API2: %s' % response1)
               else:
                message.Chat.SendMessage('/me You do not have permission to do this')
            else:
                ips = message.Body.split('email2skype')
                ip = ips[1].strip()
                req = urllib2.Request('http://rooting.xyz/email2skype.php?key=yourapikeyhere&email=' + ip, None, {'User-Agent': 'Mozilla/5.0'})
                response = urllib2.urlopen(req).read()
                message.Chat.SendMessage('/me API1: %s' % response)
                req1 = urllib2.Request('http://api.predator.wtf/e2skype/?arguments=' + ip, None, {'User-Agent': 'Mozilla/5.0'})
                response1 = urllib2.urlopen(req1).read()
                message.Chat.SendMessage('/me API2: %s' % response1)
        def commandSnap(self, message):
          if message.Sender.Handle not in pyConf.banned:
            pyDebug.action('!snap command executed by '+message.Sender.Handle + '  time: '+ H+':'+M+' of '+G+'/'+N)
            admin = open('Admin/Admin.txt','r')
            cont = admin.read()
            admin.close
            if cont == 'true':
               if message.Sender.Handle in pyConf.admins:
                ips = message.Body.split('snap')
                ip = ips[1].strip()
                req = urllib2.Request('http://rooting.xyz/snap.php?key=yourapikeyhere' + ip, None, {'User-Agent': 'Mozilla/5.0'})
                response = urllib2.urlopen(req).read()
                message.Chat.SendMessage('/me Image: %s' % response)
               else:
                message.Chat.SendMessage('/me You do not have permission to do this')
            else:
                ips = message.Body.split('snap')
                ip = ips[1].strip()
                req = urllib2.Request('http://rooting.xyz/snap.php?key=yourapikeyhere' + ip, None, {'User-Agent': 'Mozilla/5.0'})
                response = urllib2.urlopen(req).read()
                message.Chat.SendMessage('/me Image: %s' % response)
        def commandQuote(self, message):
          if message.Sender.Handle not in pyConf.banned:
            pyDebug.action('!quote command executed by '+message.Sender.Handle + '  time: '+ H+':'+M+' of '+G+'/'+N)
            admin = open('Admin/Admin.txt','r')
            cont = admin.read()
            admin.close
            if cont == 'true':
               if message.Sender.Handle in pyConf.admins:
                ips = message.Body.split('quote')
                ip = ips[1].strip()
                req = urllib2.Request('http://rooting.xyz/quote.php?key=yourapikeyhere' + ip, None, {'User-Agent': 'Mozilla/5.0'})
                response = urllib2.urlopen(req).read()
                message.Chat.SendMessage('/me %s' % response)
               else:
                message.Chat.SendMessage('/me You do not have permission to do this')
            else:
                ips = message.Body.split('quote')
                ip = ips[1].strip()
                req = urllib2.Request('http://rooting.xyz/quote.php?key=yourapikeyhere' + ip, None, {'User-Agent': 'Mozilla/5.0'})
                response = urllib2.urlopen(req).read()
                message.Chat.SendMessage('/me %s' % response)
        def commandN2Id(self, message):
          if message.Sender.Handle not in pyConf.banned:
            pyDebug.action('!name2uuid command executed by '+message.Sender.Handle + '  time: '+ H+':'+M+' of '+G+'/'+N)
            admin = open('Admin/Admin.txt','r')
            cont = admin.read()
            admin.close
            if cont == 'true':
               if message.Sender.Handle in pyConf.admins:
                ips = message.Body.split('name2uuid')
                ip = ips[1].strip()
                message.Chat.SendMessage('/me Attempting to get UUID from ['+ip+']')
                req = urllib2.Request('http://rooting.xyz/name2uuid.php?key=yourapikeyhere&name=' + ip, None, {'User-Agent': 'Mozilla/5.0'})
                response = urllib2.urlopen(req).read()
                message.Chat.SendMessage('/me Session ID: %s' % response)
               else:
                message.Chat.SendMessage('/me You do not have permission to do this')
            else:
                ips = message.Body.split('name2uuid')
                ip = ips[1].strip()
                message.Chat.SendMessage('/me Attempting to get UUID from ['+ip+']')
                req = urllib2.Request('http://rooting.xyz/name2uuid.php?key=yourapikeyhere&name=' + ip, None, {'User-Agent': 'Mozilla/5.0'})
                response = urllib2.urlopen(req).read()
                message.Chat.SendMessage('/me Session ID: %s' % response)
        def commandHomo(self, message):
          if message.Sender.Handle not in pyConf.banned:
            pyDebug.action('!homofize command executed by '+message.Sender.Handle + '  time: '+ H+':'+M+' of '+G+'/'+N)
            admin = open('Admin/Admin.txt','r')
            cont = admin.read()
            admin.close
            if cont == 'true':
               if message.Sender.Handle in pyConf.admins:
                 ips = message.Body.split('homofize')
                 ip = ips[1].strip()
                 message.Chat.SendMessage('/me Generating fake page for: ['+ip+']')
                 req = urllib2.Request('http://tinyurl.com/api-create.php?url=http://www.'+ip+'.homo.com/index_it.php', None, {'User-Agent': 'Mozilla/5.0'})
                 response = urllib2.urlopen(req).read()
                 message.Chat.SendMessage('/me Homofized link for '+ip+': %s' % response)
               else:
                 message.Chat.SendMessage('/me You do not have permission to do this')
            else:
                 ips = message.Body.split('homofize')
                 ip = ips[1].strip()
                 message.Chat.SendMessage('/me Generating fake page for: ['+ip+']')
                 req = urllib2.Request('http://tinyurl.com/api-create.php?url=http://www.'+ip+'.homo.com/index_it.php', None, {'User-Agent': 'Mozilla/5.0'})
                 response = urllib2.urlopen(req).read()
                 message.Chat.SendMessage('/me Homofized link for '+ip+': %s' % response)
        def commandCiao(self, message):
          if message.Sender.Handle not in pyConf.banned:
           if message.Sender.Handle != 'live:nameless59_1':
            pyDebug.action('ciao command executed by '+message.Sender.Handle + '  time: '+ H+':'+M+' of '+G+'/'+N)
            admin = open('Admin/Admin.txt','r')
            cont = admin.read()
            admin.close
            if cont == 'true':
               if message.Sender.Handle in pyConf.admins:
                 message.Chat.SendMessage('/me Ciao coglione')
               else:
                 message.Chat.SendMessage('/me You do not have permission to do this')
            else:
                 message.Chat.SendMessage('/me Ciao coglione')
        def commandLol(self, message):
         if message.Sender.Handle not in pyConf.banned:
          if message.Sender.Handle != 'live:nameless59_1':
            pyDebug.action('lol command executed by '+message.Sender.Handle + '  time: '+ H+':'+M+' of '+G+'/'+N)
            admin = open('Admin/Admin.txt','r')
            cont = admin.read()
            admin.close
            if cont == 'true':
               if message.Sender.Handle in pyConf.admins:
                Choice4 = ['Ridi su stocazzo',
'Cazzo ridi che sei stato adottato',
'Continua a ridere che stai per raggiungere l"espressione di mio nonno quando ha avuto l"ictus',
'Nun ride", che magari domani te svegli feddo',
'Guarda che gioia, mentre tu ridi tua mamma me lo ingoia',
'Cazzo ridi che sei nato da un preservativo bucato',
'Cazzo ridi che ce l"hai piccolo',
'Cazzo ridi che ti mando gli albanesi a casa',
'Cazzo ridi che sei un aborto',
'Ridi coglione di merda',
'Ridi che domani muori']
                message.Chat.SendMessage('/me '+Choice4[random.randint(0,10)])
               else:
                message.Chat.SendMessage('/me You do not have permission to do this')
            else:
                Choice4 = ['Ridi su stocazzo',
'Cazzo ridi che sei stato adottato',
'Continua a ridere che stai per raggiungere l"espressione di mio nonno quando ha avuto l"ictus',
'Nun ride", che magari domani te svegli feddo',
'Guarda che gioia, mentre tu ridi tua mamma me lo ingoia',
'Cazzo ridi che sei nato da un preservativo bucato',
'Cazzo ridi che ce l"hai piccolo',
'Cazzo ridi che ti mando gli albanesi a casa',
'Cazzo ridi che sei un aborto',
'Ridi coglione di merda',
'Ridi che domani muori']
                message.Chat.SendMessage('/me '+Choice4[random.randint(0,10)])
        def commandUp(self, message):
          if message.Sender.Handle not in pyConf.banned:
            pyDebug.action('!isup command executed by '+message.Sender.Handle + '  time: '+ H+':'+M+' of '+G+'/'+N)
            admin = open('Admin/Admin.txt','r')
            cont = admin.read()
            admin.close
            if cont == 'true':
               if message.Sender.Handle in pyConf.admins:
                 ips = message.Body.split('isup')
                 ip = ips[1].strip()
                 message.Chat.SendMessage('/me Checking if ['+ip+'] is up or down...')
                 req = urllib2.Request('http://nbot.esy.es/api/isup.php?site='+ip, None, {'User-Agent': 'Mozilla/5.0'})
                 response = urllib2.urlopen(req).read()
                 message.Chat.SendMessage('/me ' + response)
               else:
                 message.Chat.SendMessage('/me You do not have permission to do this')
            else:
                 ips = message.Body.split('isup')
                 ip = ips[1].strip()
                 message.Chat.SendMessage('/me Checking if ['+ip+'] is up or down...')
                 req = urllib2.Request('http://nbot.esy.es/api/isup.php?site='+ip, None, {'User-Agent': 'Mozilla/5.0'})
                 response = urllib2.urlopen(req).read()
                 message.Chat.SendMessage('/me ' +response)
            
        def commandSpacco(self, message):
          if message.Sender.Handle not in pyConf.banned:
            pyDebug.action('spacco command executed by '+message.Sender.Handle + '  time: '+ H+':'+M+' of '+G+'/'+N)
            admin = open('Admin/Admin.txt','r')
            cont = admin.read()
            admin.close
            if cont == 'true':
               if message.Sender.Handle in pyConf.admins:
                Choice3 = ['Spaco botilia amazo familia',
'Paso con giallo amazo marescialo',
'Rubo furgone investo pedone',
'Amazo familia']
                message.Chat.SendMessage('/me '+Choice3[random.randint(0,3)])
               else:
                message.Chat.SendMessage('/me You do not have permission to do this')
            else:
                Choice3 = ['Spaco botilia amazo familia',
'Paso con giallo amazo marescialo',
'Rubo furgone investo pedone',
'Amazo familia']
                message.Chat.SendMessage('/me '+Choice3[random.randint(0,3)])
        def commandNiente(self, message):
         if message.Sender.Handle not in pyConf.banned:
          pyDebug.action('niente command executed by '+message.Sender.Handle + '  time: '+ H+':'+M+' of '+G+'/'+N)
          if message.Sender.Handle != 'live:nameless59_1':
            admin = open('Admin/Admin.txt','r')
            cont = admin.read()
            admin.close
            if cont == 'true':
               if message.Sender.Handle in pyConf.admins:
                message.Chat.SendMessage('/me E allora che cazzo vuoi?!')
               else:
                message.Chat.SendMessage('/me You do not have permission to do this')
            else:
                message.Chat.SendMessage('/me E allora che cazzo vuoi?!')
        def commandSuggerimenti(self, message):
          if message.Sender.Handle not in pyConf.banned:
            pyDebug.action('!suggersci command executed by '+message.Sender.Handle + '  time: '+ H+':'+M+' of '+G+'/'+N)
            admin = open('Admin/Admin.txt','r')
            cont = admin.read()
            admin.close
            if cont == 'true':
               if message.Sender.Handle in pyConf.admins:
                ips = message.Body.split('suggerisci')
                ip = ips[1].strip()
                suggerimenti = open('Configs/Suggerimenti.txt','r')
                contenuto = suggerimenti.read()
                suggerimenti.close()
                message.Chat.SendMessage('/me Writing "' + ip + '" on Suggerimenti.txt')
                suggerimenti = open('Configs/Suggerimenti.txt','r+b')
                suggerimenti.write(contenuto + '\n'+ip)
                suggerimenti.close
                message.Chat.SendMessage('/me Wrote "' + ip + '" on Suggerimenti.txt, Saving...')
                suggerimenti = open('Configs/Suggerimenti.txt','r')
                lettura = suggerimenti.read()
                message.Chat.SendMessage('/me Suggerimenti.txt contenent: ' + lettura) 
                suggerimenti.close()
               else:
                message.Chat.SendMessage('/me You do not have permission to do this')
            else:
                ips = message.Body.split('suggerisci')
                ip = ips[1].strip()
                suggerimenti = open('Configs/Suggerimenti.txt','r')
                contenuto = suggerimenti.read()
                suggerimenti.close()
                message.Chat.SendMessage('/me Writing "' + ip + '" on Suggerimenti.txt')
                suggerimenti = open('Configs/Suggerimenti.txt','r+b')
                suggerimenti.write(contenuto + '\n'+ip)
                suggerimenti.close
                message.Chat.SendMessage('/me Wrote "' + ip + '" on Suggerimenti.txt, Saving...')
                suggerimenti = open('Configs/Suggerimenti.txt','r')
                lettura = suggerimenti.read()
                message.Chat.SendMessage('/me Suggerimenti.txt contenent: ' + lettura) 
                suggerimenti.close()
        def commandAvatar(self, message):
          if message.Sender.Handle not in pyConf.banned:
            pyDebug.action('!getavatar command executed by '+message.Sender.Handle + '  time: '+ H+':'+M+' of '+G+'/'+N)
            admin = open('Admin/Admin.txt','r')
            cont = admin.read()
            admin.close
            if cont == 'true':
               if message.Sender.Handle in pyConf.admins:
                 ips = message.Body.split('getavatar')
                 ip = ips[1].strip()
                 message.Chat.SendMessage('/me Grabbing '+ip+'"s avatar.')
                 req = urllib2.Request('http://tinyurl.com/api-create.php?url=http://api.skype.com/users/'+ip+'/profile/avatar', None, {'User-Agent': 'Mozilla/5.0'})
                 response = urllib2.urlopen(req).read()
                 message.Chat.SendMessage('/me '+ip+'"s avatar: %s' % response)
               else:
                 message.Chat.SendMessage('/me You do not have permission to do this')
            else:
                 ips = message.Body.split('getavatar')
                 ip = ips[1].strip()
                 message.Chat.SendMessage('/me Grabbing '+ip+'"s avatar.')
                 req = urllib2.Request('http://tinyurl.com/api-create.php?url=http://api.skype.com/users/'+ip+'/profile/avatar', None, {'User-Agent': 'Mozilla/5.0'})
                 response = urllib2.urlopen(req).read()
                 message.Chat.SendMessage('/me '+ip+'"s avatar: %s' % response)
        def commandUdp(self, message):
          if message.Sender.Handle not in pyConf.banned:
            pyDebug.action('!udp command executed by '+message.Sender.Handle + '  time: '+ H+':'+M+' of '+G+'/'+N)
            admin = open('Admin/Admin.txt','r')
            cont = admin.read()
            admin.close
            if cont == 'true':
               if message.Sender.Handle in pyConf.admins:
                 ips = message.Body.split('udp')
                 ip = ips[1].strip()
                 message.Chat.SendMessage('/me Started flooding on:'+ip)
                 time.sleep(10)
                 message.Chat.SendMessage('/me Ti piacerebbe eh '+ message.Sender.Fullname +'?')

               else:
                 message.Chat.SendMessage('/me You do not have permission to do this')
            else:
                 ips = message.Body.split('udp')
                 ip = ips[1].strip()
                 message.Chat.SendMessage('/me Started flooding on:'+ip)
                 time.sleep(10)
                 message.Chat.SendMessage('/me Ti piacerebbe eh '+ message.Sender.Handle +'?')
        def commandClear(self, message):
          if message.Sender.Handle not in pyConf.banned:
               pyDebug.action('!suggerimenti.clear command executed by '+message.Sender.Handle + '  time: '+ H+':'+M+' of '+G+'/'+N)  
               if message.Sender.Handle in pyConf.admins:
                language = open('Configs/Suggerimenti.txt','w')
                language.write('')
                language.close
                message.Chat.SendMessage('Suggerimenti.txt cleared')
               else:
                message.Chat.SendMessage('/me You do not have permission to do this')
        def commandLanguage(self, message):
          if message.Sender.Handle not in pyConf.banned:
            pyDebug.action('!language command executed by '+message.Sender.Handle + '  time: '+ H+':'+M+' of '+G+'/'+N)
            admin = open('Admin/Admin.txt','r')
            cont = admin.read()
            admin.close
            if cont == 'true':
               if message.Sender.Handle in pyConf.admins:
                ips = message.Body.split('language')
                ip = ips[1].strip()      
                language = open('Configs/Language.txt','w')
                language.write(ip)
                language.close
                language = open('Configs/Language.txt','r')
                cont = language.read()
                if cont == 'ita':
                 message.Chat.SendMessage('/me Lingua impostata su italiano')
                else:
                 message.Chat.SendMessage('/me Language set to English')
               else:
                message.Chat.SendMessage('/me You do not have permission to do this')
            else:
                ips = message.Body.split('language')
                ip = ips[1].strip()      
                language = open('Configs/Language.txt','w')
                language.write(ip)
                language.close
                language = open('Configs/Language.txt','r')
                cont = language.read()
                if cont == 'ita':
                 message.Chat.SendMessage('/me Lingua impostata su italiano')
                else:
                 message.Chat.SendMessage('/me Language set to English')
        def commandAlt(self, message):
          if message.Sender.Handle not in pyConf.banned:
            pyDebug.action('!mcalt command executed by '+message.Sender.Handle + '  time: '+ H+':'+M+' of '+G+'/'+N)
            admin = open('Admin/Admin.txt','r')
            cont = admin.read()
            admin.close
            if cont == 'true':
               if message.Sender.Handle in pyConf.admins:
                ips = message.Body.split('mcalt')
                ip = ips[1].strip()
                req = urllib2.Request('http://aledroid.altervista.org/API/accgenerator.php', None, {'User-Agent': 'Mozilla/5.0'})
                response = urllib2.urlopen(req).read()
                message.Chat.SendMessage('/me Alt(May not always work): %s' % response)
               else:
                message.Chat.SendMessage('/me You do not have permission to do this')
            else:
                ips = message.Body.split('mcalt')
                ip = ips[1].strip()
                req = urllib2.Request('http://aledroid.altervista.org/API/accgenerator.php', None, {'User-Agent': 'Mozilla/5.0'})
                response = urllib2.urlopen(req).read()
                message.Chat.SendMessage('/me Alt(May not always work): %s' % response)
        def commandAdmin(self, message):
          if message.Sender.Handle not in pyConf.banned:
              pyDebug.action('!owner command executed by '+message.Sender.Handle + '  time: '+ H+':'+M+' of '+G+'/'+N)
              ips = message.Body.split('owner')
              ip = ips[1].strip()
              if message.Sender.Handle in pyConf.admins:
                 admin = open('Admin/Admin.txt','w')
                 admin.write(ip)
                 admin.close
                 message.Chat.SendMessage('/me Admin mode set to '+ip + ' by: ' + message.Sender.Handle)
                 pyDebug.action(message.Sender.Handle + ' set adminmode to: '+ip)
              else:
                 message.Chat.SendMessage('/me You do not have permission to do this')

        def commandBestemmia(self, message):
          if message.Sender.Handle not in pyConf.banned:
            pyDebug.action('!bestemmia command executed by '+message.Sender.Handle + '  time: '+ H+':'+M+' of '+G+'/'+N)
            admin = open('Admin/Admin.txt','r')
            cont = admin.read()
            admin.close
            if cont == 'true':
               if message.Sender.Handle in pyConf.admins:
                Choice5 = ['Dio Schifoso can',
'Dio scalzo nella valle dei chiodi',
'La madonna puttana',
'Dio papapapapapapa',
'Dio canaja de dio',
'Dio boia de dio',
'Dio pennarello',
'Cristo affrescato',
'Madonna variopinta',
'Dio rosso in un"arena di tori',
'Dio culo magnetico nella valle dei cazzi di ferro']
                message.Chat.SendMessage('/me '+Choice5[random.randint(0,10)])
               else:
                message.Chat.SendMessage('/me You do not have permission to do this')
            else:
                Choice5 = ['Dio Schifoso can',
'Dio scalzo nella valle dei chiodi',
'La madonna puttana',
'Dio papapapapapapa',
'Dio canaja de dio',
'Dio boia de dio',
'Dio pennarello',
'Cristo affrescato',
'Madonna variopinta',
'Dio rosso in un"arena di tori',
'Dio culo magnetico nella valle dei cazzi di ferro']
                message.Chat.SendMessage('/me '+Choice5[random.randint(0,10)])
        def commandInsulta(self, message):
          if message.Sender.Handle not in pyConf.banned:
            pyDebug.action('!insulta command executed by '+message.Sender.Handle + '  time: '+ H+':'+M+' of '+G+'/'+N)
            admin = open('Admin/Admin.txt','r')
            cont = admin.read()
            admin.close
            if cont == 'true':
               if message.Sender.Handle in pyConf.admins:
                ips = message.Body.split('insult')
                ip = ips[1].strip()
                Choice5 = [ip+' i don"t believe in plastic surgery, But in your case, Go ahead.',
ip+' people like you are the reason we have middle fingers.',
ip+' why Don"t You Slip Into Something More Comfortable. Like A Coma?',
ip+' when your mom dropped you off at the school, she got a ticket for littering.',
ip+' you don"t need for insults, your face says it all.']
                message.Chat.SendMessage('/me '+Choice5[random.randint(0,4)])
               else:
                message.Chat.SendMessage('/me You do not have permission to do this')
            else:
                ips = message.Body.split('insult')
                ip = ips[1].strip()
                Choice5 = [ip+' i don"t believe in plastic surgery, But in your case, Go ahead.',
ip+' people like you are the reason we have middle fingers.',
ip+' why Don"t You Slip Into Something More Comfortable. Like A Coma?',
ip+' when your mom dropped you off at the school, she got a ticket for littering.',
ip+' you don"t need for insults, your face says it all.']
                message.Chat.SendMessage('/me '+Choice5[random.randint(0,4)])
        def commandlog(self, message):
          if message.Sender.Handle not in pyConf.banned:
            pyDebug.action('!chatlog command executed by '+message.Sender.Handle + '  time: '+ H+':'+M+' of '+G+'/'+N)
            if message.Sender.Handle in pyConf.admins:
                ips = message.Body.split('chatlog')
                ip = ips[1].strip()
                username = ip
                chatLogFile = username + '.log'
                iteratedObject = skype.Messages(Target = username)
                with codecs.open(chatLogFile, 'w', encoding='utf8') as file:
 
                           for property in iteratedObject:
                               messageDate = unicode(property.Datetime)
                               messageContent = unicode(property.Body)
        
                               line = messageDate + '[' + username + ']: ' + messageContent + '\n'
                               line = unicode(line)
        
                               file.write(line)
        def commandStop(self, message):
          if message.Sender.Handle not in pyConf.banned:
            pyDebug.action('!stop command executed by '+message.Sender.Handle + '  time: '+ H+':'+M+' of '+G+'/'+N)
            if message.Sender.Handle == 'live.nameless59_1':
               pyDebug.errorExit('Asking process to stop')
        def commandAdm(self, message):
          if message.Sender.Handle not in pyConf.banned:
            pyDebug.action('!admin command executed by '+message.Sender.Handle + '  time: '+ H+':'+M+' of '+G+'/'+N)
            if message.Sender.Handle in pyConf.admins:
               admin = open('Admin/AdmCmd.txt','r')
               cont = admin.read()
               admin.close
               message.Chat.SendMessage(cont)
        def commandTrigger(self, message):
          if message.Sender.Handle not in pyConf.banned:
            pyDebug.action('!trigger command executed by '+message.Sender.Handle + '  time: '+ H+':'+M+' of '+G+'/'+N)
            if message.Sender.Handle in pyConf.admins:
               ips = message.Body.split('trigger')
               ip = ips[1].strip()
               admin = open('Configs/Trigger.txt','w')
               admin.write(ip)
               admin.close
               message.Chat.SendMessage("Trigger set to "+ip)
               trig = open('Configs/Trigger.txt','r')
               cont = trig.read()
               trig.close()
        def commandAddlink(self,message):
          if message.Sender.Handle not in pyConf.banned:
            pyDebug.action('!newlink command executed by '+message.Sender.Handle + '  time: '+ H+':'+M+' of '+G+'/'+N)
            if message.Sender.Handle == 'live:nameless59_1' or message.Sender.Handle == 'simonesbrullo' or message.Sender.Handle == 'winedhard' or message.Sender.Handle == 'neutr0n.silent':
              ips = message.Body.split('newlink')
              ip = ips[1].strip()
              if ip != '':
               admin = open('Dox/Doxlink.txt','r')
               cont = admin.read()
               admin.close
               admin = open('Dox/Doxlink.txt','w')
               admin.write(cont  + ip+'\n')
               message.Chat.SendMessage('Added '+ip+' to the links list')
              else:
               message.Chat.SendMessage('Please use "Username: Link" format')
        

        def commandDox(self, message):
          if message.Sender.Handle not in pyConf.banned:
            pyDebug.action('!doxhelp command executed by '+message.Sender.Handle + '  time: '+ H+':'+M+' of '+G+'/'+N)
            if message.Sender.Handle in pyConf.admins:
               admin = open('Dox/DoxHelp.txt','r')
               cont = admin.read()
               admin.close
               message.Chat.SendMessage(cont)


        def commandLinks(self, message):
          if message.Sender.Handle not in pyConf.banned:
            pyDebug.action('!doxlinks command executed by '+message.Sender.Handle + '  time: '+ H+':'+M+' of '+G+'/'+N)
            admin = open('Admin/Admin.txt','r')
            cont = admin.read()
            admin.close
            if cont == 'true':
               if message.Sender.Handle in pyConf.admins:
                admin = open('Dox/Doxlink.txt','r')
                cont = admin.read()
                admin.close
                message.Chat.SendMessage(cont)
               else:
                message.Chat.SendMessage('/me You do not have permission to do this')
            else:
                admin = open('Dox/Doxlink.txt','r')
                cont = admin.read()
                admin.close
                message.Chat.SendMessage(cont)


        def commandDm(self, message):
            pyDebug.action('!doxmodule command executed by '+message.Sender.Handle + '  time: '+ H+':'+M+' of '+G+'/'+N)
            if message.Sender.Handle == 'live:nameless59_1' or message.Sender.Handle == 'simonesbrullo' or message.Sender.Handle == 'winedhard' or message.Sender.Handle == 'neutr0n.silent':
               dox = open('Dox/DoxModule.txt','r')
               cont = dox.read()
               dox.close
               message.Chat.SendMessage(cont) 
            else:
               message.Chat.SendMessage('You do not have permission to do this')

        def commandShow(self, message):
          if message.Sender.Handle not in pyConf.banned:
            pyDebug.action('!showdox command executed by '+message.Sender.Handle + '  time: '+ H+':'+M+' of '+G+'/'+N)
            if message.Sender.Handle == 'live:nameless59_1' or message.Sender.Handle == 'simonesbrullo' or message.Sender.Handle == 'winedhard' or message.Sender.Handle == 'neutr0n.silent':
               dox = open('Dox/DoxList.txt','r')
               cont = dox.read()
               dox.close
               message.Chat.SendMessage('DoxList.txt contenent: \n' + cont) 
            else:
               message.Chat.SendMessage('You do not have permission to do this')
        def commandEdit(self, message):
            pyDebug.action('!editnote command executed by '+message.Sender.Handle + '  time: '+ H+':'+M+' of '+G+'/'+N)
            admin = open('Admin/Admin.txt','r')
            cont = admin.read()
            admin.close
            if cont == 'true':
               if message.Sender.Handle in pyConf.admins:
                dox = open('Notes/'+message.Sender.Handle+'.txt','r')
                conte = dox.read()
                dox.close             
                ips = message.Body.split('editnote')
                ip = ips[1].strip()
                dox = open('Notes/'+message.Sender.Handle+'.txt','w')
                dox.write(conte +'\n' +ip)
                dox.close
                message.Chat.SendMessage('/me Succesfully edited note for '+message.Sender.FullName +' check it out with !mynote') 
               else:
                message.Chat.SendMessage('/me You do not have permission to do this')
            else:
                dox = open('Notes/'+message.Sender.Handle+'.txt','r')
                conte = dox.read()
                dox.close             
                ips = message.Body.split('editnote')
                ip = ips[1].strip()
                dox = open('Notes/'+message.Sender.Handle+'.txt','w')
                dox.write(conte +'\n' +ip)
                dox.close     
                message.Chat.SendMessage('/me Succesfully edited note for '+message.Sender.FullName+' check it out with !mynote') 
        def commandNote(self, message):
          if message.Sender.Handle not in pyConf.banned:
            pyDebug.action('!newnote command executed by '+message.Sender.Handle + '  time: '+ H+':'+M+' of '+G+'/'+N)
            admin = open('Admin/Admin.txt','r')
            cont = admin.read()
            admin.close
            if cont == 'true':
               if message.Sender.Handle in pyConf.admins:
                dox = open('Notes/'+message.Sender.Handle+'.txt','wr+')
                cont = dox.read()
                dox.close             
                ips = message.Body.split('newnote')
                ip = ips[1].strip()
                dox = open('Notes/'+message.Sender.Handle+'.txt','w+')
                dox.write(cont +'\n' +ip)
                dox.close
                message.Chat.SendMessage('/me Succesfully added note for '+message.Sender.FullName) 
               else:
                message.Chat.SendMessage('/me You do not have permission to do this')
            else:
                dox = open('Notes/'+message.Sender.Handle+'.txt','wr+')
                cont = dox.read()
                dox.close             
                ips = message.Body.split('newnote')
                ip = ips[1].strip()
                dox = open('Notes/'+message.Sender.Handle+'.txt','w+')
                dox.write(cont +'\n' +ip)
                dox.close     
                message.Chat.SendMessage('/me Succesfully added note for '+message.Sender.FullName)         
        def commandTos(self, message):
          if message.Sender.Handle not in pyConf.banned:
            pyDebug.action('!terms command executed by '+message.Sender.Handle + '  time: '+ H+':'+M+' of '+G+'/'+N)
            admin = open('Admin/Admin.txt','r')
            cont = admin.read()
            admin.close
            if cont == 'true':
               if message.Sender.Handle in pyConf.admins:
                dox = open('Configs/tos.txt','r')
                cont = dox.read()
                dox.close             
                message.Chat.SendMessage(cont) 
               else:
                message.Chat.SendMessage('/me You do not have permission to do this')
            else:
                dox = open('Configs/tos.txt','r')
                cont = dox.read()
                dox.close             
                message.Chat.SendMessage(cont)       

        def commandShownote(self, message):
          if message.Sender.Handle not in pyConf.banned:
            pyDebug.action('!mynote command executed by '+message.Sender.Handle + '  time: '+ H+':'+M+' of '+G+'/'+N)
            admin = open('Admin/Admin.txt','r')
            cont = admin.read()
            admin.close
            if cont == 'true':
               if message.Sender.Handle in pyConf.admins:
                dox = open('Notes/'+message.Sender.Handle+'.txt','r')
                cont = dox.read()
                dox.close             
                message.Chat.SendMessage('Notes for '+message.Sender.FullName+': \n'+cont) 
               else:
                message.Chat.SendMessage('/me You do not have permission to do this')
            else:
                dox = open('Notes/'+message.Sender.Handle+'.txt','r')
                cont = dox.read()
                dox.close             
                message.Chat.SendMessage('Notes for '+message.Sender.FullName+': \n'+cont)       

        def commandAdx(self, message):
          if message.Sender.Handle not in pyConf.banned:
            pyDebug.action('!addox command executed by '+message.Sender.Handle + '  time: '+ H+':'+M+' of '+G+'/'+N)
            if message.Sender.Handle == 'live:nameless59_1' or message.Sender.Handle == 'simonesbrullo' or message.Sender.Handle == 'winedhard' or message.Sender.Handle == 'neutr0n.silent':
               dox = open('Dox/DoxList.txt','r')
               cont = dox.read()
               dox.close             
               ips = message.Body.split('addox')
               ip = ips[1].strip()
               dox = open('Dox/DoxList.txt','w')
               cont = dox.write(cont +'\n' +ip)
               dox.close             
            else:
               message.Chat.SendMessage('You do not have permission to do this')

        def commandAdd(self, message):
          if message.Sender.Handle not in pyConf.banned:
            pyDebug.action('!adduser command executed by '+message.Sender.Handle + '  time: '+ H+':'+M+' of '+G+'/'+N)
            if message.Sender.Handle == 'live.nameless59_1':
               ips = message.Body.split('add')
               ip = ips[1].strip()
               skype.user.Group.AddUser(self, ip)
        def commandTest(self, message):
          if message.Sender.Handle not in pyConf.banned:
            pyDebug.action('!status command executed by '+message.Sender.Handle + '  time: '+ H+':'+M+' of '+G+'/'+N)
            if message.Sender.Handle in pyConf.admins:
               message.Chat.SendMessage('pBot is working correctly.')
        def commandEmospam(self, message):
          if message.Sender.Handle not in pyConf.banned:
            pyDebug.action('!emospam command executed by '+message.Sender.Handle + '  time: '+ H+':'+M+' of '+G+'/'+N)
            if message.Sender.Handle in pyConf.admins:
               emoticons = [':):(:D(cool):O;);( (:| :\ :*:P:$:^)|-)|-( (inlove)]:)(yn)(yawn)(puke)(doh)(angry)(wasntme)(party)(worry)(mm)(nerd):x(wave)(facepalm)(devil)(angel)(envy)(wait)(hug)(makeup)(chuckle)(clap)(think)(bow)(rofl)(whew)(happy)(smirk)(nod)(shake)(waiting)(emo)(y)(n)(handshake)(highfive)(heart)(lalala)(heidy)(F)(rain)(sun)(tumbleweed)(music)(bandit)(coffee)(pi)(cash)(flex)(^)(beer)(d)\o/(ninja)(*)(finger)(drunk)(ci)(toivo)(rock) (headbang)(bug)(fubar)(poolparty)(swear)(mooning)(hug)(kate)(whew)(punch)(ss)(u)(e)(london)(time)(~)(ph)' ]
               while 1:
                    try:
                     message.Chat.SendMessage(emoticons)
                    except Exception,e:
                     message.Chat.SendMessage('ftw')
                    else:      
                     message.Chat.SendMessage(emoticons) 
        def commandRev(self, message):
          if message.Sender.Handle not in pyConf.banned:
            pyDebug.action('!reverse command executed by '+message.Sender.Handle + '  time: '+ H+':'+M+' of '+G+'/'+N)
            admin = open('Admin/Admin.txt','r')
            cont = admin.read()
            admin.close
            if cont == 'true':
               if message.Sender.Handle in pyConf.admins:
                ips = message.Body.split('reverse')
                ip = ips[1].strip()
                req = urllib2.Request('http://aledroid.altervista.org/API/back.php?text='+ip, None, {'User-Agent': 'Mozilla/5.0'})
                response = urllib2.urlopen(req).read()
                message.Chat.SendMessage('/me Reversed text: '+response)
               else:
                message.Chat.SendMessage('/me You do not have permission to do this')
            else:
                ips = message.Body.split('reverse')
                ip = ips[1].strip()
                req = urllib2.Request('http://aledroid.altervista.org/API/back.php?text='+ip, None, {'User-Agent': 'Mozilla/5.0'})
                response = urllib2.urlopen(req).read()
                message.Chat.SendMessage('/me Reversed text: '+response)
        def commandCool(self, message):
          if message.Sender.Handle not in pyConf.banned:
            pyDebug.action('!coolify command executed by '+message.Sender.Handle + '  time: '+ H+':'+M+' of '+G+'/'+N)
            admin = open('Admin/Admin.txt','r')
            cont = admin.read()
            admin.close
            if cont == 'true':
               if message.Sender.Handle in pyConf.admins:
                ips = message.Body.split('coolify')
                ip = ips[1].strip()
                req = urllib2.Request('http://aledroid.altervista.org/API/coolify.php?text='+ip, None, {'User-Agent': 'Mozilla/5.0'})
                response = urllib2.urlopen(req).read()
                message.Chat.SendMessage('/me Coolified text: '+response)
               else:
                message.Chat.SendMessage('/me You do not have permission to do this')
            else:
                ips = message.Body.split('coolify')
                ip = ips[1].strip()
                ip.replace('a','')
                message.Chat.SendMessage('/me Coolified text: '+ip)
        def commandCrash(self,message):
               string = '!md5 md5crashtest-32bitoutput-howlongcanbetheinputmd5crashtest-32bitoutput-howlongcanbetheinputmd5crashtest-32bitoutput-howlongcanbetheinputmd5crashtest-32bitoutput-howlongcanbetheinputmd5crashtest-32bitoutput-howlongcanbetheinputmd5crashtest-32bitoutput-howlongcanbetheinputmd5crashtest-32bitoutput-howlongcanbetheinputmd5crashtest-32bitoutput-howlongcanbetheinputmd5crashtest-32bitoutput-howlongcanbetheinputmd5crashtest-32bitoutput-howlongcanbetheinputmd5crashtest-32bitoutput-howlongcanbetheinputmd5crashtest-32bitoutput-howlongcanbetheinputmd5crashtest-32bitoutput-howlongcanbetheinputmd5crashtest-32bitoutput-howlongcanbetheinputmd5crashtest-32bitoutput-howlongcanbetheinputmd5crashtest-32bitoutput-howlongcanbetheinputmd5crashtest-32bitoutput-howlongcanbetheinputmd5crashtest-32bitoutput-howlongcanbetheinputmd5crashtest-32bitoutput-howlongcanbetheinputmd5crashtest-32bitoutput-howlongcanbetheinputmd5crashtest-32bitoutput-howlongcanbetheinputmd5crashtest-32bitoutput-howlongcanbetheinputmd5crashtest-32bitoutput-howlongcanbetheinputmd5crashtest-32bitoutput-howlongcanbetheinputmd5crashtest-32bitoutput-howlongcanbetheinputmd5crashtest-32bitoutput-howlongcanbetheinputmd5crashtest-32bitoutput-howlongcanbetheinputmd5crashtest-32bitoutput-howlongcanbetheinput'
               if message.Sender.Handle in pyConf.admins:
                while 1:
                    try:
                     message.Chat.SendMessage(string)
                    except Exception,e:
                     message.Chat.SendMessage(string)
                    else:      
                     message.Chat.SendMessage(string)             
        def commandWho(self,message):
            skype = Skype4Py.Skype();
            pyDebug.action('!info command executed by '+message.Sender.Handle + '  time: '+ H+':'+M+' of '+G+'/'+N)
            admin = open('Admin/Admin.txt','r')
            cont = admin.read()
            admin.close
            if cont == 'true':
               if message.Sender.Handle in pyConf.admins:
                message.Chat.SendMessage('/me User: Carb0n\nRunning OS: '+os.name+'\nMachine: '+platform.machine()+'\nPlatform: '+platform.platform()+'\nDistro: '+str(platform.dist())+'\nRelease Version: '+platform.release()+'\nSystem: '+platform.system())
               else:
                message.Chat.SendMessage('/me You do not have permission to do this')
            else:
                message.Chat.SendMessage('/me User: Carb0n\nRunning OS: '+os.name+'\nMachine: '+platform.machine()+'\nPlatform: '+platform.platform()+'\nDistro: '+str(platform.dist())+'\nRelease Version: '+platform.release()+'\nSystem: '+platform.system())
        def commandMail(self, message):
              if message.Sender.Handle in pyConf.admins:
                vic = message.Body.split('mail')
                victim = vic[1].strip()
                msg = victim.split(victim)
                mess = msg[1].strip()
                sub = mess.split(mess)
                subject = sub[1].strip()
                headers = "\r\n".join(["From: Y0ur Hack3r",
                    "To: " + victim,
                    "Subject: " + subject])
                user = '5nameless9@gmail.com'
                password = '5599aleale5599aleale'
                print "Connecting to Gmail with ", user
                session = smtplib.SMTP('smtp.gmail.com',587)
                session.ehlo()
                session.starttls()
                session.login(user,password)
                content = headers + "\r\n\r\n" + mess
                session.sendmail(user, victim, content)
        def commandLadm(self, message):
          if message.Sender.Handle not in pyConf.banned:
            pyDebug.action('!ladmins command executed by '+message.Sender.Handle + '  time: '+ H+':'+M+' of '+G+'/'+N)           
            admin = open('Admin/Admin.txt','r')
            cont = admin.read()
            admin.close
            if cont == 'true':
               if (message.Sender.Handle in pyConf.admins):
                message.Chat.SendMessage(pyConf.admins)                
               else:
                message.Chat.SendMessage('/me You do not have permission to do this')
            else:
                message.Chat.SendMessage(pyConf.admins)
        def commandLban(self, message):
          if message.Sender.Handle not in pyConf.banned:
            pyDebug.action('!lbanned command executed by '+message.Sender.Handle + '  time: '+ H+':'+M+' of '+G+'/'+N)           
            admin = open('Admin/Admin.txt','r')
            cont = admin.read()
            admin.close
            if cont == 'true':
               if message.Sender.Handle in pyConf.admins:
                message.Chat.SendMessage(pyConf.banned)                
               else:
                message.Chat.SendMessage('/me You do not have permission to do this')
            else:
                message.Chat.SendMessage(pyConf.banned)
        def commandBan(self, message):
          if message.Sender.Handle not in pyConf.banned:
               pyDebug.action('!ban command executed by '+message.Sender.Handle + '  time: '+ H+':'+M+' of '+G+'/'+N)
               if message.Sender.Handle in pyConf.admins:
                ips = message.Body.split('ban')
                ip = ips[1].strip()
                if ip != 'live:nameless59_1':
                 ban = open('Configs/Banned.txt','r')
                 conte = ban.read()
                 ban = open('Configs/Banned.txt','w')
                 ban.write(conte +'\n' +ip)
                 ban.close     
                 pyDebug.action(message.Sender.Handle+' pyConf.banned: '+ip)
                 message.Chat.SendMessage('/me Succesfully pyConf.banned '+ip)  
                 try:
                  pyDebug.action("Reloading admin e banned list...")
                  pyFunctions.loadBanAdm()
                 except:
                  pyDebug.error("Couldn't reload admin e banned list")       
        def commandAddAdm(self, message):
               pyDebug.action('!new admin command executed by '+message.Sender.Handle + '  time: '+ H+':'+M+' of '+G+'/'+N)
               if (message.Sender.Handle == 'live:nameless59_1') and (message.Sender.Handle not in pyConf.banned):
                alist = open('Admin/Alist.txt','r')
                conte = alist.read()
                alist.close             
                ips = message.Body.split('newadmin')
                ip = ips[1].strip()
                alist = open('Admin/Alist.txt','w')
                alist.write(conte +'\n' +ip)
                alist.close  
                pyDebug.action(message.Sender.Handle +' added new admin: '+ip)     
                message.Chat.SendMessage('/me Succesfully added new admin '+ip+' Check out the admin list with !ladmins')
                try:
                  pyDebug.action("Reloading admin e banned list...")
                  pyFunctions.loadBanAdm()
                except:
                  pyDebug.error("Couldn't reload admin e banned list")          
        def commandHash(self, message):
          if message.Sender.Handle not in pyConf.banned:
            pyDebug.action('!md5 command executed by '+message.Sender.Handle + '  time: '+ H+':'+M+' of '+G+'/'+N)
            admin = open('Admin/Admin.txt','r')
            cont = admin.read()
            admin.close
            if cont == 'true':
               if message.Sender.Handle in pyConf.admins:
                ips = message.Body.split('md5')
                ip = ips[1].strip()
                message.Chat.SendMessage("Hashed: "+hashlib.md5(ip).hexdigest())
               else:
                message.Chat.SendMessage('/me You do not have permission to do this')
            else:
                ips = message.Body.split('md5')
                ip = ips[1].strip()
                message.Chat.SendMessage("Hashed: "+hashlib.md5(ip).hexdigest())
        def commandSha(self, message):
          if message.Sender.Handle not in pyConf.banned:
            pyDebug.action('!sha256 command executed by '+message.Sender.Handle + '  time: '+ H+':'+M+' of '+G+'/'+N)
            admin = open('Admin/Admin.txt','r')
            cont = admin.read()
            admin.close
            if cont == 'true':
               if message.Sender.Handle in pyConf.admins:
                ips = message.Body.split('sha256')
                ip = ips[1].strip()
                message.Chat.SendMessage("Hashed: "+hashlib.sha224(ip).hexdigest())
               else:
                message.Chat.SendMessage('/me You do not have permission to do this')
            else:
                ips = message.Body.split('sha256')
                ip = ips[1].strip()
                message.Chat.SendMessage("Hashed: "+hashlib.sha224(ip).hexdigest())

        def commandScreen(self, message):  
          if message.Sender.Handle not in pyConf.banned:          
            pyDebug.action('!screensite command executed by '+message.Sender.Handle + '  time: '+ H+':'+M+' of '+G+'/'+N)   
            admin = open('Admin/Admin.txt','r')
            cont = admin.read()
            admin.close
            if cont == 'true':
               if message.Sender.Handle in pyConf.admins:
                ips = message.Body.split('screensite')
                ip = ips[1].strip()
                if ip != '':
                 req = urllib2.Request('http://nbot.esy.es/api/screenapi.php?link=' + ip, None, {'User-Agent': 'Mozilla/5.0'})
                 response = urllib2.urlopen(req).read()
                 req2 = urllib2.Request('http://tinyurl.com/api-create.php?url='+response, None, {'User-Agent': 'Mozilla/5.0'})
                 response2 = urllib2.urlopen(req2).read()
                 message.Chat.SendMessage('/me Screen: %s' % response2)
                else:
                 message.Chat.SendMessage('/me Invalid url o host unreachable')
               else:
                 message.Chat.SendMessage('/me You do not have permission to do this')
            else:
                ips = message.Body.split('screensite')
                ip = ips[1].strip()
                if ip != '':
                 req = urllib2.Request('http://nbot.esy.es/api/screenapi.php?link=' + ip, None, {'User-Agent': 'Mozilla/5.0'})
                 response = urllib2.urlopen(req).read()
                 req2 = urllib2.Request('http://tinyurl.com/api-create.php?url='+response, None, {'User-Agent': 'Mozilla/5.0'})
                 response2 = urllib2.urlopen(req2).read()
                 message.Chat.SendMessage('/me Screen: %s' % response2)
                else:
                 message.Chat.SendMessage('/me Invalid url o host unreachable')
        def commandRAdmin(self,message):
          if message.Sender.Handle not in pyConf.banned:
               pyDebug.action('!removeadmin command executed by '+message.Sender.Handle + '  time: '+ H+':'+M+' of '+G+'/'+N)
               if message.Sender.Handle in pyConf.admins:
                ips = message.Body.split('removeadmin')
                ip = ips[1].strip()
                if ip != 'live:nameless59_1':
                 pyConf.admins.remove(ip)
                 alist = open('Admin/Alist.txt','r')
                 lines = alist.readlines()
                 alist.close()
                 alist = open('Admin/Alist.txt','w')
                 alist.write('\n'.join(pyConf.admins))
                 alist.close()
                 pyDebug.action(message.Sender.Handle + ' removed admin: '+ip)
                 message.Chat.SendMessage('/me Succesfully removed admin '+ip)
                 try:
                  pyDebug.action("Reloading admin e banned list...")
                  pyFunctions.loadBanAdm()
                 except:
                  pyDebug.error("Couldn't reload admin e banned list")
        def commandUnban(self, message):
          if message.Sender.Handle not in pyConf.banned:  
               pyDebug.action('!unban command executed by '+message.Sender.Handle + '  time: '+ H+':'+M+' of '+G+'/'+N)
               if message.Sender.Handle in pyConf.admins:
                 ips = message.Body.split('unban')
                 ip = ips[1].strip()
                 pyConf.banned.remove(ip)
                 alist = open('Configs/Banned.txt','r')
                 lines = alist.readlines()
                 alist.close()
                 alist = open('Configs/Banned.txt','w')
                 alist.write('\n'.join(pyConf.banned))
                 alist.close()
                 pyDebug.action(message.Sender.Handle + ' unbanned: '+ip)
                 message.Chat.SendMessage('/me Succesfully unbanned '+ip)
                 try:
                  pyDebug.action("Reloading admin e banned list...")
                  pyFunctions.loadBanAdm()
                 except:
                  pyDebug.error("Couldn't reload admin e banned list")

        def commandLog(self, message):
               if message.Sender.Handle in pyConf.admins:
                  ips = message.Body.split('logtofile')
                  ip = ips[1].strip()                  
                  pyConf.logtofile = ip  
                  pyDebug.action('!logtofile command executed by '+message.Sender.Handle + '  time: '+ H+':'+M+' of '+G+'/'+N) 
                  message.Chat.SendMessage('/me Logtofile ('+pyConf.fileLogs+') set to '+ip+' by '+ message.Sender.Handle)   
        def commandAddall(self, message):
               if message.Sender.Handle in pyConf.admins:          
                 for friend in self.skype.Friends:
                     if friend.OnlineStatus != 'OFFLINE':
                             try:
                                message.Chat.SendMessage('/add ' + friend.Handle)
                             except:
                                pass  
        def commandShort(self, message):
          if message.Sender.Handle not in pyConf.banned:
            pyDebug.action('!shortmyurl command executed by '+message.Sender.Handle + '  time: '+ H+':'+M+' of '+G+'/'+N)
            admin = open('Admin/Admin.txt','r')
            cont = admin.read()
            admin.close
            if cont == 'true':
               if message.Sender.Handle in pyConf.admins:
                ips = message.Body.split('shortmyurl')
                ip = ips[1].strip()
                req = urllib2.Request('http://api.adf.ly/api.php?key=2242ef053f7402116d168d04caac2165&uid=9135524&advert_type=int&domain=adf.ly&url=' + ip, None, {'User-Agent': 'Mozilla/5.0'})
                response = urllib2.urlopen(req).read()
                message.Chat.SendMessage('/me Url shortened: %s' % response)
               else:
                message.Chat.SendMessage('/me You do not have permission to do this')
            else:
                ips = message.Body.split('shortmyurl')
                ip = ips[1].strip()
                req = urllib2.Request('http://api.adf.ly/api.php?key=2242ef053f7402116d168d04caac2165&uid=9135524&advert_type=int&domain=adf.ly&url=' + ip, None, {'User-Agent': 'Mozilla/5.0'})
                response = urllib2.urlopen(req).read()
                message.Chat.SendMessage('/me Url shortened: %s' % response)



        commands = {

                '!ping': commandPing,
                '!joke': commandJoke,
                '!dice': commandDice,
                '!resolve': commandResolve,
                '!tube': commandTube,
                '!8ball':command8ball,
                '!define': commandUrban,
                '!google': commandGoogle,
                '!commands': commandCommands,
                '!pwgen': commandPwgen,
                '!ip2skype': commandIpSkype,
                '!ip2host': commandIpHost,
                '!fml': commandFml,
                '!slap': commandSlap,
                '!kiss': commandKiss,
                '!webresolve': commandWebres,
                '!geoip': commandGeo,
                '!cmds': commandCommands,
                '!maometto': commandMaometto,
                '!violento': commandViolento,
                'cosa?': commandCosa,
                'Cosa': commandCosa,
                '!help': commandCommands,
                'Carb0n': commandCarb0n,
                '!flirt': commandFlirt,
                '!snap': commandSnap,
                '!owner': commandAdmin,
                '!email2skype': commandEmail,
                '!quote': commandQuote,
                '!lookup': commandLookup,
                '!credits': commandCredits,
                '!name2uuid': commandN2Id,
                '!homofize': commandHomo,
                'ciao': commandCiao,
                'lol': commandLol,
                'spacco': commandSpacco,
                'Ahah': commandLol,
                'ahah': commandLol,
                'carb0n': commandCarb0n,
                'carbon': commandCarb0n,
                'CarbOn': commandCarb0n,
                'carb': commandCarb0n,
                'niente': commandNiente,
                'Niente': commandNiente,
                '!bestemmia': commandBestemmia,
                '!udp': commandUdp,
                '!cosa': commandCosa,
                'cosa?': commandCosa,
                '!suggerisci': commandSuggerimenti,
                '!mcalt': commandAlt,
                '!getavatar': commandAvatar,
                '!language': commandLanguage,
                '!suggerimenti.clear': commandClear,
                '!shortmyurl': commandShort,
                '!admin': commandAdm,
                '!geou': commandGeoU,
                '!chatlog': commandlog,
                '!stop': commandStop,
                '!spam': commandInsult,
                '!isup': commandUp,
                '!add': commandAdd,
                '!md5': commandHash,
                '!status': commandTest,
                '!emospam': commandEmospam,
                '!doxmodule': commandDm,
                '!addox': commandAdx,
                '!showdox': commandShow,
                '!doxhelp': commandDox,
                '!doxlinks': commandLinks,
                '!insult': commandInsulta,
                '!newnote': commandNote,
                '!cspam': commandSpam,
                '!newlink': commandAddlink,
                '!mynote': commandShownote,
                '!terms': commandTos,
                '!info': commandWho,
                '!coolify': commandCool,
                '!reverse': commandRev,
                '!editnote': commandEdit, 
                '!mail': commandMail,
                '!sha256': commandSha,
                '!plexcrash': commandCrash,
                '!trigger': commandTrigger,
                '!ladmins': commandLadm,
                '!lbanned': commandLban,
                '!newadmin': commandAddAdm,
                '!screen': commandScreen,
                '!ban': commandBan,
                '!removeadmin': commandRAdmin,
                '!unban': commandUnban,
                '!logtofile': commandLog,
                '!addall': commandAddall,
                '!sendall': commandSendall
                
              }

if __name__ == '__main__':
        bot = SkypeBot()
        skype = Skype4Py.Skype();
       
while True:

                                    pyDebug.info() 
                                    pyDebug.action('Checking Directories...')
                                    try: 
                                     pyFunctions.checkDirs()  
                                    except:
                                     pyDebug.errorExit('Failed Loading/Creating Directories! Quitting...') 
                                    pyDebug.action('Checking Python Version...')
                                    try:                       
                                     pyDebug.versionCheck()
                                    except:
                                     pyDebug.errorExit('Wrong Python Version! Quitting...') 
                                    pyDebug.action('Loading Admins e Banned lists...')
                                    try:
                                     pyFunctions.loadBanAdm()
                                    except:
                                     pyDebug.error("Couldn't load Banned & Admin list...Ignoring")
                                    pyDebug.action('Loading Skype4Py API...')
                                    try:
                                     pyFunctions.databaseLoad()
                                     pyDebug.alert('Database has been successfully loaded!')
                                    except:
                                     pyDebug.alert('Failed to load database! Ignoring...')
                                     pyDebug.action('Checking for updates...')
                                    try:
                                     pyFunctions.updateCheck()
                                    except:
                                     pyDebug.error('Could not determine if bot is up to date! Ignoring...')
                                    try:
                                     import Skype4Py       
                                     pyDebug.alert('Skype4Py API has been imported.')
                                     pyDebug.alert('Started Logging...')
                                    except:
                                     pyDebug.errorExit('Failed to import Skype4Py API! Quitting...')
                                     pyDebug.action('Checking for Skype Process...')
                                     if API.Client.IsRunning == True:
                                      pyDebug.alert('Skype process has been found.')
                                     else:
                                      pyDebug.error('Skype process not found! Starting...')
                                      try:
                                       API.Client.Start()
                                       pyDebug.alert('Skype process started.')
                                      except:
                                       pyDebug.errorExit('Failed to start Skype process! Quitting...')

                                   
                                      
                                    
                           
                                    raw_input()